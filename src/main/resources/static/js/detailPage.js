
		
		var sNow = $("#studySNow").val();
		var sTotal = $("#studySTotal").val();
		console.log(sNow + ", " + sTotal + sNow >= sTotal);
		if (parseInt(sNow) >= parseInt(sTotal)) {

			$("#btnJoinStudy").addClass("btn btn-danger flex-shrink-0");
			$("#btnJoinStudy").text("모집마감");
			$("#btnJoinStudy").attr("disabled", true);

		}
		var flag = false;
		
		var token = $("meta[name='_csrf']").attr("content");
		var header = $("meta[name='_csrf_header']").attr("content");

		console.log("token+header : " + token + ", " + header);

		$(document).ajaxSend(function(e, xhr, options) {
			xhr.setRequestHeader(header, token);
		})

		var sNo = $("#studySNo").val();

		var paramData = {
			"sNo" : sNo
		};

		console.log("paramData : " + JSON.stringify(paramData));

		$.ajax({

			url : "/selectStudyReview",
			type : "post",
			contentType : "application/json",
			dataType : "json",
			data : JSON.stringify(paramData),
			beforeSend : function(xhr) { /*데이터를 전송하기 전에 헤더에 csrf값을 설정한다*/
				xhr.setRequestHeader(header, token);
			},
			async : false,
			cache : false,
			success : function(data) {
				//alert("리뷰조회성공");
				if (flag == false) {
					for ( var i = 0 in data) {
						$("#reviewTbody").append(
								$("<tr>" + "<th>" + data[i].SR_NO + "</th>"
										+ "<th>" + data[i].SR_WRITE_DATE
										+ "</th>" 
										+ "<th>" + data[i].SR_CONTENT
										+ "</th>" 
										+ "<th>" 
										  + "<div class='star-ratings'>"
										  +"<div class='star-ratings-fill space-x-2 text-lg' id='divBGrade2"+i+"'>"
										  +"<span class='starIcon'>★</span><span>★</span><span>★</span><span>★</span><span>★</span>" 
										  +"</div>"
										  +"<div class='star-ratings-base space-x-2 text-lg'>"
										  +"<span>★</span><span>★</span><span>★</span><span>★</span><span>★</span>"
										  +"</div>"
										  +"</div>"
										+ "</th>" 
										+ "<th>" 
										+ data[i].USER_NAME
										+ "</th>" 
										+ "</tr>"));
						var grade = data[i].SR_GRADE/5 * 100;
						var x = "width : "+grade+"%";
						console.log("data[i].SR_GRADE :"+data[i].SR_GRADE+", x : "+x+", grade : "+grade);
						$("#divBGrade2"+i).attr('style',"width : "+grade+"%");
					}
				}
				
				
				flag = true;

						$.ajax({
			
						url : "/selectStudyReviewOne",
						type : "post",
						contentType : "application/json",
						dataType : "json",
						data : JSON.stringify(paramData),
						beforeSend : function(xhr) { //데이터를 전송하기 전에 헤더에 csrf값을 설정한다
							xhr.setRequestHeader(header, token);
						},
						async : false,
						cache : false,
						success : function(data) {

							if(data.SM_NO == null){
								//alert("스터디원이 아닙니다.");
								$("#btnQuitStudy").hide();
							}else{
								//alert("스터디원 입니다.");
								
								if(data.SR_NO == null && data.SM_ROLE == '스터디원'){
									//alert("리뷰를 등록한적이 없습니다.(스터디원)");
									$("#btnQuitStudy").show();
									
								}else if(data.SR_NO != null && data.SM_ROLE == '스터디원'){
									//alert("리뷰를 등록했습니다.(스터디원)");
									$("#btnQuitStudy").show();
									$("#btnQuitStudy").html("등록한 리뷰");
									$("#btnQuitStudy").attr("disabled",true);
								}else{
									//alert("스터디장"+data.SM_ROLE);
									$("#btnQuitStudy").hide();
								}
							
							}
						},
						error : function() {
							//alert("실패");
						}
					})



				
			},
			error : function() {
				alert("리뷰조회 실패");
			}
		});
		
	
	
	
	
	//var token = $("meta[name='_csrf']").attr("content");
	//var header = $("meta[name='_csrf_header']").attr("content");
	
	
	

	var modal = document.querySelector(".modal");

	
	$(document).on("click", "#insertReview", function(event){
	
	
		console.log("하이룽");

		var srContent = $("#reviewText").val();
		var srGrade = $('input[name="starpoint"]:checked').val();
		console.log("srContent : "+srContent+",srGrade : "+srGrade );
		var sNo = $("#studySNo").val();
		var smNo = $("#studySmNo").val();
		var review = {
			"srContent" : srContent,
			"srGrade" : srGrade,
			"sNo" : sNo,
			"smNo" : smNo
		};

		console.log(JSON.stringify(review));

		$.ajax({

			url : "/insertStudyReview",
			type : "post",
			contentType : "application/json",
			dataType : "json",
			data : JSON.stringify(review),
			beforeSend : function(xhr) { //데이터를 전송하기 전에 헤더에 csrf값을 설정한다
				xhr.setRequestHeader(header, token);
			},
			async : false,
			cache : false,
			success : function(data) {
				alert("리뷰가 작성되었습니다.");
				$('#closeModal').click();
				location.reload();
			},
			error : function() {
				alert("실패");
			}
		})

	
	
	});

	

	var flag = false;
   	
   	
   	
   	
	
	$("#btnReply").click(function() {

		var token = $("meta[name='_csrf']").attr("content");
		var header = $("meta[name='_csrf_header']").attr("content");

		console.log("token+header : " + token + ", " + header);

		$(document).ajaxSend(function(e, xhr, options) {
			xhr.setRequestHeader(header, token);
		})

		var sNo = $("#studySNo").val();

		var sc_content = $("#reply").val();

		if (sc_content == null) {
			alert("댓글 내용을 입력해주세요");
			return false;
		}

		var paramData = {
			"sNo" : sNo,
			"scContent" : sc_content
		};

		$.ajax({

			url : "/insertStudyComment",
			type : "post",
			contentType : "application/json",
			dataType : "json",
			data : JSON.stringify(paramData),
			beforeSend : function(xhr) { /*데이터를 전송하기 전에 헤더에 csrf값을 설정한다*/
				xhr.setRequestHeader(header, token);
			},
			async : false,
			cache : false,
			success : function(data) {
				
				Swal.fire({
				  title :'댓글이 등록되었습니다.',
				  icon: 'success',
				  }).then((result) => {
					if(result.isConfirmed){
					 
					 location.reload();						
					}	
				});
				
			},
			error : function() {
				alert("댓글 추가 실패");
			}

		});

	});

	$(".modifyComment").click(function() {

						var str = "";
						var tdArr = new Array();
						var checkBtn = $(this);
						var tr = checkBtn.parent().parent();
						var td = tr.children();
						var rid = td.eq(0).text();
						var modify = $(".modifyComment").val();
						
						//var studyCommentNo = $("input[id='hello']").val();

						console.log(" tr : " + tr.text() + ", td : "
								+ td.text() + ", rid : " + rid);

						$("#btnModfiyComment" + rid).remove();
						//$("#deleteComment").remove();

						var replyInfo = $("#commentScContent" + rid);
						var input = $("#commentContentList" + rid).children(
								"input[type=text]");

						var buttonReply = '<button type="button" class="btn-sm btn-primary d-inline-block" id="replyUpdate">등록</button>';
						var buttonReplyCancel = '<button type="button" class="btn-sm btn-danger d-inline-block" id="replyUpdateCancel">취소</button>';

						$("#btnModifyContainer" + rid).append(
								buttonReply + "&nbsp;");
						$("#btnModifyContainer" + rid)
								.append(buttonReplyCancel);

						var updateReply = $(this).next();

						$(this).css("display", "none");
						updateReply.css("display", "inline-block");
						input.attr("readonly", false)
								.css("background", "white");
						replyInfo.css("background", "rgb(252, 252, 253)").css(
								"border", "1px solid #ddd").css("box-shadow",
								"0 2px 3px rgba(0,0,0,0.3)");
						input.focus();

						$("#replyUpdate")
								.click(
										function() {

											var scNo = $(
													"#studyCommentNo" + rid)
													.val();

											var commentScContent = $(
													"#commentScContent" + rid)
													.val();
											
											var userNo = $(".loginUserSession").val();
											
											var sNo = $("#studySNo").val();
											
											var updateComment = {
												"scNo" : scNo,
												"scContent" : commentScContent,
												"userNo" : userNo,
												"sNo" : sNo
											};

											console.log(updateComment);

											$
													.ajax({
														url : "/updateStudyComment",
														type : "post",
														dataType : "json",
														contentType : "application/json",
														data : JSON
																.stringify(updateComment),
														success : function(data) {
															console
																	.log("data :"
																			+ data.scContent);
															
															location.reload();
														},
														error : function() {
															alert("댓글 수정 실패");
														}

													});

										})

						$("#replyUpdateCancel").click(function() {
							location.reload();
						})

					});
	
	//댓글 삭제 비동기 통신
	$(".deleteComment").click(function() {

		var result = confirm("댓글을 삭제하시겠습니까?");

		if (result) {
			
			var str = "";
			var tdArr = new Array();
			var checkBtn = $(this);
			var tr = checkBtn.parent().parent();
			var td = tr.children();
			var rid = td.eq(0).text();
			var userNo = $(".loginUserSession").val();
			var scNo = $("#studyCommentNo" + rid).val();
			var sNo = $("#studySNo").val();
			
			console.log(" tr : " + tr.text() + ", td : "
					+ td.text() + ", rid : " + rid+", userNo : "+userNo);
			
			var dataParam = {
				"scNo" : scNo,
				"userNo" : userNo,
				"sNo" : sNo
			}
			console.log("댓글 삭제 dataParm : "+JSON.stringify(dataParam));
				
			$.ajax({

				url : "/deleteStudyComment",
				type : "post",
				dataType : "json",
				contentType : "application/json",
				data : JSON.stringify(dataParam),
				success : function(data) {
					location.reload();
					//$(this).remove();
									  
				},
				error : function() {
					console.log("댓글 삭제 실패");
				}

			});

		
		}
	});

	//참가자 정보 조회 비동기 통신
 	$("#liveToastBtn").click(function(){
		
		var sNo = $("#studySNo").val();
		
		var paramData = {"sNo" : sNo};
		console.log("ajax 실행");
		$.ajax({
			
			url : "/selectStudyMember",
			type : "post",
			dataType : "json",
			contentType : "application/json",
			data : JSON.stringify(paramData),
			success:function(data){
				
				console.log(data);
				if(flag == false){
				for ( var i = 0 in data) {
				$(".toast-body").append($("<ul class='list-group'>"
										 +"<li class='list-group-item disabled'><img src='"+data[i].USER_PROFILEIMG+"' style='width : 50px; height: 50px;' class='rounded-circle'>"+" "+data[i].USER_NAME+" | "+data[i].USER_ID+"</li>"));
					}
				}
				$(".toast-body").append($("</ul>"));
				flag = true;
				
			},error : function(){
				
				alert("통신 실패");
				
			}
			
		});  
		
		
		
	});
	
	$(document).ready(function (){
			
		  $("#liveToastBtn").click(function(){
		        $("#liveToast").toast("show");
		    });
		  			
	}); 
	
	
	$("#deleteStudy").click(function(){	
		
	Swal.fire({
		  title: '스터디를 삭제하시겠습니까?',
		  text: "스터디는 다시 복구되지 않습니다.",
		  icon: 'warning',
		  showCancelButton: true,
		  confirmButtonColor: '#3085d6',
		  cancelButtonColor: '#d33',
		  confirmButtonText: '삭제'
		}).then((result) => {
		  if (result.isConfirmed) {
		    Swal.fire(
		      'Deleted!',
		      '삭제되었습니다.',
		      'success'
		    )
		    
		    var sNo = $("#studySNo").val();
		    var smNo = $("#studySmNo").val(); 
		    
		    var paramData = {"sNo" : sNo, "smNo" : smNo}; 
		    console.log(paramData);
		      $.ajax({
		    	
		    	  url : "/deleteStudy",
					type : "post",
					dataType : "json",
					contentType : "application/json",
					data : JSON.stringify(paramData),
					success:function(data){
					 	console.log("삭제성공");
					 	location.href="/mainPage";
					},
					error:function(){
						
					}
		    	  
		    	  
		      });
		    
		  }
		})
		
	});
	
	$("#btnQuitStudy").click(function(){
		
		var userNo = $("#userNo").val();
		var smNo = $("#userSmNo").val(); 
		
		var paramData = {"userNo" : userNo, "smNo" : smNo};
		
		$.ajax({
			
			 url : "/exitStudy",
					type : "post",
					dataType : "json",
					contentType : "application/json",
					data : JSON.stringify(paramData),
					success:function(data){
							

							//$("#endStudy").trigger("click");
							$('#reviewModal').modal('show');
							
							$("#insertReview").click(function(){
							
								$("#btnQuitStudy").attr("disabled", true);
								$("#btnQuitStudy").html("탈퇴한 스터디입니다.");
									
							});
							
							
							
							
					},error:function(){
						
					}
			
		});
		
	})
	