/*!
* Start Bootstrap - Modern Business v5.0.5 (https://startbootstrap.com/template-overviews/modern-business)
* Copyright 2013-2021 Start Bootstrap
* Licensed under MIT (https://github.com/StartBootstrap/startbootstrap-modern-business/blob/master/LICENSE)
*/
// This file is intentionally blank
// Use this file to add JavaScript to your project

$.ajax({
	type:'POST',
	url:'/api/v1/',
	dataType:'json',
	contentType:'application/json; charset=utf-8',
	data: JSON.stringify(data)
}).done(function(){
	alert('글이 등록되었습니다.');
	window.location.href = '/';
}).fail(function (error){
	alert(JSON.stringify(error));
});