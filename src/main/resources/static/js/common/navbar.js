webSocketOpen();

function webSocketOpen(){
	webSocket = new WebSocket("ws://" + location.host + "/alarm");
	console.log(webSocket);
	webSocketEvt();			
	}


	function webSocketEvt(){
		console.log("이벤트시작");
		webSocket.onopen = function(data){
			
			
		}
		
		webSocket.onmessage = function(data){
			
			var msg = data.data;
			
			console.log("message정보 : "+msg);
			
			// 모달 알림
			let toast = "<div class='toast hide' role='alert' aria-live='assertive' aria-atomic='true'>";
		    toast += "<div class='toast-header'><i class='fas fa-bell mr-2'></i><strong class='mr-auto'>알림</strong>";
		    toast += "<small class='text-muted'>just now</small><button type='button' class='btn-close' data-bs-dismiss='toast' aria-label='Close'></button></div> ";		    													
		    //toast += "<span aria-hidden='true'>&times;</span>";
		    toast += "<div class='toast-body'>" + msg + "</div></div>";
		    $("#toastMessage").append(toast);   // msgStack div에 생성한 toast 추가
		    $(".toast").toast({"animation": true, "autohide": false});
		    $('.toast').toast('show');
			
			
		}
								
			
	  }
	  
	 $("#gdsImg").change(function(){
		 if(this.files && this.files[0]) {
		  var reader = new FileReader;
		  reader.onload = function(data) {
		   $(".select_img img").attr("src", data.target.result).width(300);        
		  }
		  reader.readAsDataURL(this.files[0]);
		 }
		}); 