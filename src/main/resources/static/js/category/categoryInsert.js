   var token = $("meta[name='_csrf']").attr("content");
	var header = $("meta[name='_csrf_header']").attr("content");

	console.log("token+header : " + token + ", " + header);

	$(document).ajaxSend(function(e, xhr, options) {
		xhr.setRequestHeader(header, token);
	});

	$("#divSelectCategory").hide();
	$("#divInsertCategory").hide();
	
    
	$("#selectCategory").click(function(){						//카테고리 조회 클릭
 		if($("#divSelectCategory").is(":visible")){
			$("#divSelectCategory").slideUp();
			
		}else{
			
			$("#divSelectCategory").slideDown();
			
		}
    });
	
    $("#insertCategory").click(function(){						//카테고리 추가 클릭
    		if($("#divInsertCategory").is(":visible")){
    			$("#divInsertCategory").slideUp();
    			
    		}else{
    			$("#divInsertCategory").slideDown();
    			
    		}
    	
    });
    
    
    
    
    var flag = false;
    
    $.ajax({
		url : "/selectCategoryAdmin"
		,type : "post"
		,dataType: "json"
		,contentType : "application/json"
		,beforeSend : function(xhr) { /*데이터를 전송하기 전에 헤더에 csrf값을 설정한다*/
		 	xhr.setRequestHeader(header, token);
		}
		,async : false
		,cache : false
		,success : function(data){
			//alert("통신 성공");
			
			if(flag == false){	
				for(var i=0 in data){
				$(".mylist").append($("<li class='left filterBtn mx-2' value='"+data[i].CATEGORY_NO+"'>"
									   +"<input type='hidden' id='liCategoryNo' value='"+data[i].CATEGORY_NO+"' class='selectCategory"+(i+1)+"'>" 
									   +"<input type='checkbox' id='cb"+i+"' value='"+data[i].CATEGORY_NO+"' name='categoryNo' />"
									   +"<label for='cb"+i+"'>"
									   +"<img src='"+data[i].CATEGORY_THUMBIMG+"' style='width: 50px; height: 60px;' class='rounded-circle' />"
									   +"</label>"
									   +"<br>"
									   +"<span class='jg-font' style='font-size: 1em; color:black;'>"+data[i].CATEGORY_NAME+"</span>"+"</li>"));
				}
			}
			
			flag = true;
		}
		,error : function(){
			alrt("통신 실패");
		}
	});
    
 	
    $("#categoryImg").change(function(){
		console.log("이미지변경");
		if(this.files && this.files[0]){
			var reader = new FileReader;
			reader.onload = function(data){
			$(".select_img img").attr("src", data.target.result).width(500);
			}
		reader.readAsDataURL(this.files[0]);
		
		}
	});
    
	$('input[type="checkbox"][name="categoryNo"]').click(function(){
		
		if($(this).prop('checked')){
			$('input[type="checkbox"][name="categoryNo"]').prop('checked',false);
			
			$(this).prop('checked', true);
		}
	});
	
    
    
	$(function(){						//유저 우클릭 이벤트
		
		$.contextMenu({							
	        selector: '.left',
	        trigger: 'left',
	        callback: function(key, options) {
	            var m = "clicked: " + key;
	           
	            //window.console && console.log(m) || alert(m);
	        },
	        items: {
	        	
	        	/*" modify": {name : "수정", icon : "fas fa-exclamation-circle", callback : function(key, options){
						alert("경고 클릭");	            	
	            }}, */
	            "delete": {name : "삭제", icon : "fas fa-minus-circle" , callback : function(key, options){
	            		var categoryNo = $(this).val();
	            	
	            		paramData = {"categoryNo" : categoryNo};
	            		
	            		alert("카테고리 삭제");
	            		$.ajax({
							
							url : "/deleteCategory"					//카테고리 삭제기능
							,type : "post"
							,dataType: "json"
							,contentType : "application/json"
							,beforeSend : function(xhr) { /*데이터를 전송하기 전에 헤더에 csrf값을 설정한다*/
								xhr.setRequestHeader(header, token);
							 }
							,data : JSON.stringify(paramData)
							,async : false
							,cache : false
							,success : function(data){
								
								location.reload();
								
							},error : function(){
								alert("카테고리 삭제 통신 실패");
							}
							
							});
				}},
	            "sep1": "---------",
	            "quit": {name : "Quit", icon : function($element, key, item){ return 'context-menu-icon context-menu-icon-quit'; }}
	        }
	    });


	});	
    