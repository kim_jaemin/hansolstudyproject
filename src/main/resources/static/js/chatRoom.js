	var token = $("meta[name='_csrf']").attr("content");
	var header = $("meta[name='_csrf_header']").attr("content");
	$(document).ajaxSend(function(e, xhr, options) {
		xhr.setRequestHeader(header, token);
	})

	var webSocket;
	window.onload = function(){
		getRoom();
		createRoom();
	}
	
	function getRoom(){
		console.log("getRoom 실행 : ");
		var paramData = { sNo : $("#studySNo").val(), smNo : $("#studySmNo").val()};
		commonAjax("/getRoom", paramData,'post', function(result){
			createChatingRoom(result);
		});
	}
	
	function createRoom(){
		$("#createRoom").click(function(){
			var msg = { roomName : $("#roomName").val(),
			 	  		     sNo : $("#studySNo").val(),
			 	  		    smNo : $("#studySmNo").val()  };
			
			
			console.log("msg : "+JSON.stringify(msg));
			
			commonAjax("/createRoom", msg, "post", function(result){
				createChatingRoom(result);
			});
			
			$("#roomName").val("");
		});
	}
	
	function goRoom(smNo, sNo, roomNumber, rn){
		//location.href="/moveChating?roomName="+name+"&"+"roomNumber="+number;
		window.open("/moveChating?smNo="+smNo+"&"+"sNo="+sNo+"&roomNumber="+roomNumber+"&roomName="+rn, 'windowPop', 'width=800, height=600, left=400, top=100, resizable = yes')
	}
	
	function createChatingRoom(res){
		
		console.log("res 정보 : "+typeof res);
				
		if(res != ""){
			$(".inputTable").hide();
			
			var tag = "<tr><th class='room'>방 이름</th><th class='go'></th></tr>";
				var rn = res.roomName;
				var roomNumber = res.roomNumber;
				var smNo = $("#studySmNo").val();
				var sNo = $("#studySNo").val();
				
				console.log("rn 정보 : "+rn+", roomNumber : "+roomNumber);
				
				tag += "<tr>"
					   
					   +"<td class='room'>"+rn+"</td>"
					   +"<td class='go'><button type='button' onclick='goRoom(\""+smNo+"\",\""+sNo+"\",\""+roomNumber+"\",\""+rn+"\")'>참여</button></td>"
					   
					   +"</tr>";
				 
			$("#roomList").empty().append(tag);
		}
	}
	
	function commonAjax(url, parameter, type, callback, contentType){
			$.ajax({
				 url : url,
				data : parameter,
				type : type,
		 contentType : contentType != null?contentType:'application/x-www-form-urlencoded; charset=UTF-8',
		  beforeSend : function(xhr) { /*데이터를 전송하기 전에 헤더에 csrf값을 설정한다*/
				xhr.setRequestHeader(header, token);
			 },
		     success : function(res){
				 console.log('ajax 실행');
		    	 callback(res);
		     },
		     error : function(err){
		    	 console.log('error');
		    	 callback(err);
		     }
			});
	}