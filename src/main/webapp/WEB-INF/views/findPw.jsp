<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="en">
<!-- 로그인 페이지를 구현하기 위해 대시보드에서 제공하는 login.html이다. 김민정 -->

<!-- 1. element: <div class="col-lg-6 d-none d-lg-block bg-login-image"></div> 
2. sb-admin-2.min.css 
3. 이미지 소스파일 : https://source.unsplash.com/K4mSJ7kc0As/600x800 -->

<head>

	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport"
		content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta name="description" content="">
	<meta name="author" content="">
	<meta name="_csrf" id="_csrf" content="${_csrf.token}" />
	<meta name="_csrf_header" id="_csrf_header"
	content="${_csrf.headerName}" />

<title>StudyMoa Find Password</title>
<!-- 제이쿼리를 쓰기위한 스크립트 -->

<script src="https://code.jquery.com/jquery-3.6.0.js"
	integrity="sha256-H+K7U5CnXl1h5ywQfKtSj8PCmoN9aaq30gDh27Xc0jk="
	crossorigin="anonymous"></script>

<!-- sweet alert -->
<script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>

<!-- Custom fonts for this template-->
<link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet"
	type="text/css">
<link
	href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i"
	rel="stylesheet">
<!-- 에이작스를 쓰기위한 스크립트 -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

<!-- Custom styles for this template-->
<link href="css/styles.css" rel="stylesheet" />
<link href="css/sb-admin-2.css" rel="stylesheet">
<link href="css/common.css" rel="stylesheet">

<%-- <%@ include file="common/header.jsp"%> --%>
</head>

<body class="bg-dark">

	<div class="container">

		<!-- Outer Row -->
		<div class="row justify-content-center">

			<div class="col-xl-10 col-lg-12 col-md-9">

				<div class="card o-hidden border-0 shadow-lg my-5" style="border:solid 0px !important;padding:0px !important;">
					<div class="card-body p-0">
						<!-- Nested Row within Card Body -->
						<div class="row" style="padding-bottom:0px !important;">
							<div class="loginImage col-lg-6 d-none d-lg-block"></div>
							<div class="col-lg-6">
								<div class="p-5">
									<div class="text-center">
										<h1 class="h4 text-gray-900 mb-4">비밀번호 찾기</h1>
									</div>


									<form class="user" id="userFindPwForm" name="userFindPwForm"
										action="${pageContext.request.contextPath}/updateNewPwd"
										method="post">


										<div class="form-group">
											<span class="jg-font" style="font-size: 15px;font-weight:600;color:#5a5c69!important;">이름</span>	
											<input type="text" class="form-control form-control-user"
												id="userName" name="userName" aria-describedby="emailHelp"
												placeholder="이름">
										</div>

										<div class="form-group">
										<span class="jg-font" style="font-size: 15px;font-weight:600;color:#5a5c69!important;">아이디</span>
											<input type="email" class="form-control form-control-user"
												id="userId" name="userId" aria-describedby="emailHelp"
												placeholder="아이디">


										</div>

<br><hr><br>
										<input class="btn btn-primary btn-user btn-block"
											type="submit" id="findPwBtn" value="패스워드찾기" name="findPwBtn">
										<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" /> <br> 
											<a class="btn btn-primary btn-user btn-block" id="BackLoginClick">
                                            돌아가기 </a>

										



									</form>


								</div>
							</div>
						</div>
					</div>
				</div>

			</div>

		</div>

	</div>

	<!-- Bootstrap core JavaScript-->
	<script src="vendor/jquery/jquery.min.js"></script>
	<script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

	<!-- Core plugin JavaScript-->
	<script src="vendor/jquery-easing/jquery.easing.min.js"></script>

	<!-- Custom scripts for all pages-->
	<script src="js/sb-admin-2.min.js"></script>

</body>


<script>
$(document).ready(function(){

	
	
	var token = $("meta[name='_csrf']").attr("content");
	var header = $("meta[name='_csrf_header']").attr("content");
	
	console.log("token+header : " + token + ", " + header);
	
	$(document).ajaxSend(function(e, xhr, options) {
		xhr.setRequestHeader(header, token);
	});

	
	$("#findPwBtn").attr("disabled",true);
	//userName -> userId
	$("#userName").blur(function(){
		
		var userName = $("#userName").val();
		
		if(userName == ""){											//이름이 빈값이면
			console.log("이름이 빈값이다");
			alert("이름을 확인해주세요");
			$("#findPwBtn").attr("disabled",true);
		}else{															//이름이 빈값이 아니면
			$("#userId").blur(function(){								//폰 값 확인
				var userId =  $("#userId").val();
				
				if(userId == ""){
					alert("아이디를 확인해주세요");
					$("#findPwBtn").attr("disabled",true);
				}else{
				$("#findPwBtn").attr("disabled",false);					//이름과 폰 값이 null이 아닌경우
				}
			});	
			
		}

		});																	//userName.blur
	

$("#findPwBtn").click(function(){
	
	document.querySelector("#userFindPwForm").addEventListener('submit', function(e) {
        e.preventDefault();

      
    });
	
	
	var userName = $("#userName").val();
	var userId = $("#userId").val();
	
	var paramData = {
			"userName" : userName,
			"userId" : userId
			};
	console.log(JSON.stringify(paramData));
	
	$.ajax({
		
		url :"${pageContext.request.contextPath}/updateNewPwd",
		type : "post",
		dataType:"json",
		data : JSON.stringify(paramData),
		contentType : 'application/json',
		beforeSend : function(xhr) { /*데이터를 전송하기 전에 헤더에 csrf값을 설정한다*/
			xhr.setRequestHeader(header, token);
		},
		
		cache : false,
		success:function(data){
			console.log("데이터는 :"+data.updateNewPwd0);
			
			if(data != null){				//임시비밀번호로 업데이트하여 가져온 값이 있는경우
				
				Swal.fire({title: "임시 패스워드로 변경되었습니다!",
						   text: "임시 패스워드는 "+JSON.stringify(data.updateNewPwd0)+" 로 변경되었습니다.",
						   icon: "info",																	//버튼 종류는 warning, success, info, "error"가 있다.
						   buttons: true,
						   dangerMode: true,
						 })
						 .then((willDelete) => {
						   if (!willDelete) {
						     swal("계속 로그인을 진행해주세요", {
						       icon: "success",
						     });
						   } 
						 });
					
						}else{											//임시비밀번호를 업데이트하여 가져온 값이 없는경우
							
							
							
							Swal.fire({
								   title: "임시 비밀번호가 발급되지 않았습니다.",
								   text: "이름과 아이디를 다시 체크해주시기 바랍니다.",
								   icon: "warning",
								   buttons: "패스워드 다시찾기",
								  
								 });
						
						}
					},
					error : function(){
						alert("통신실패");
					}
				
					
				});	
				
				
				
				
			
	
	

	
});																				//버튼 클릭시
	


	$("#BackLoginClick").click(function(){
		
		history.back();
		
		
	});
	
	/* function BackLoginClick(){
		//페이지 이동 : javascript (자바스크립트)
		
		//location.href="/login"; // 우리가 컨트롤러 단에서 이미 view단 매핑을 시켜줬기 때문에 별도의 /loginPage.jsp를 해 줄 필요가 없다.[2021.10.31 김민정]
		history.back();
	} */
	
});	
	
</script>
</html>