<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>	
<!DOCTYPE html>
<html lang="en">
<!-- 로그인 페이지를 구현하기 위해 대시보드에서 제공하는 login.html이다. 김민정 -->

<!-- 1. element: <div class="col-lg-6 d-none d-lg-block bg-login-image"></div> 
2. sb-admin-2.min.css 
3. 이미지 소스파일 : https://source.unsplash.com/K4mSJ7kc0As/600x800 -->

<head>

<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no">
<meta name="description" content="">
<meta name="author" content="">

<!-- csrf토큰 403해결 -->
<meta name="_csrf" id="_csrf" content="${_csrf.token}" />
<meta name="_csrf_header" id="_csrf_header"
	content="${_csrf.headerName}" />

<title>StudyMoa JOIN PAGE</title>


<!-- Custom fonts for this template-->
<link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet"
	type="text/css">
<link
	href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i"
	rel="stylesheet">
<script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>

<link rel="icon" type="image/x-icon" href="assets/favicon.ico" />
<!-- Bootstrap icons-->
<link
	href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.5.0/font/bootstrap-icons.css"
	rel="stylesheet" />


<!-- jquery 라이브러리를 쓰기위한 스크립트명시 -->
<script src="https://code.jquery.com/jquery-3.6.0.js"
	integrity="sha256-H+K7U5CnXl1h5ywQfKtSj8PCmoN9aaq30gDh27Xc0jk="
	crossorigin="anonymous"></script>
	
	<!-- ajax jQuery  -->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
	<!-- Core theme CSS (includes Bootstrap)-->

	<link href="css/styles.css" rel="stylesheet" />
    <link href="css/sb-admin-2.css" rel="stylesheet">
    <link href="css/common.css" rel="stylesheet">
	
</head>

<body class="bg-dark">
	<!-- <style> #joinBtn{ margin-right:-15px; } </style> -->

	<div class="container">

		<!-- Outer Row -->
		<div class="row justify-content-center">

			<div class="col-xl-10 col-lg-12 col-md-9">

				<div class="card o-hidden border-0 shadow-lg my-5" style="border:solid 0px !important;padding:0px !important;">
					<div class="card-body p-0">
						<!-- Nested Row within Card Body -->
						<div class="row" style="padding-bottom:0px !important;">
							<div class="loginImage col-lg-6 d-none d-lg-block"></div>
							<div class="col-lg-6">
							  
							  
							      
							  <form class="user" action="${pageContext.request.contextPath}/joinForm" id="userInputForm" method="post" autocomplete="off"> <!-- action태그가 -->
								<div class="p-5">
									<div class="text-center">
										<h1 class="h4 text-gray-900 mb-4">회원가입</h1>
									</div>
										<div class="form-group">
										<span class="jg-font" style="font-size: 15px;font-weight:600;color:#5a5c69!important;">아이디</span><br>
										<span style= "font-size: 5pt"><c:out value="아이디는 이메일 형식으로 입력해주세요."></c:out></span>
											<input type="email" class="form-control form-control-user"
												id="userId" name="userId" aria-describedby="emailHelp"
												placeholder="아이디 (이메일 기입)" required>
												
												 
												 <!-- 나중에 아이디 중복검사시 onclick함수 사용해서 밑에 script 추가하기 
												<a style="width: 150px; height: 50px;"
													class="btn btn-primary btn-user btn-block" > 아이디 중복확인 </a>-->
												
												<span style="color:red; font-size: small" id="idNullCheck"><c:out value="${valid_userId}" ></c:out></span>
												
	
										</div>
										<div class="form-group">
										<span class="jg-font" style="font-size: 15px;font-weight:600;color:#5a5c69!important;">이름</span>
											<input type="text" class="form-control form-control-user"
												id="userName" name="userName" placeholder="이름"  required >
												<span style="color:red; font-size: small"><c:out value="${valid_userName}"></c:out></span>
										</div>


										<div class="form-group">
										<span class="jg-font" style="font-size: 15px;font-weight:600;color:#5a5c69!important;">전화번호</span>
											<input type="tel" class="form-control form-control-user"
												id="userPhone" name="userPhone" placeholder="전화번호" required>
												<span style="color:red; font-size: small"><c:out value="${valid_userPhone}"></c:out></span>
										</div>


										
										<div class="form-group">
										<span class="jg-font" style="font-size: 15px;font-weight:600;color:#5a5c69!important;">패스워드</span><br>
										<span style="font-size: 5pt"><c:out value="대소문자 관계없이 포함된 4자 ~ 20자를 입력해주세요."></c:out></span>
											<input type="password" class="form-control form-control-user"
												id="userPwd" name="userPwd" placeholder="패스워드" required>
												
												<span style="color:red; font-size: small" id="validPwd"><c:out value="${valid_userPwd}"></c:out></span>
												
										</div>


										<div class="form-group">
										<span class="jg-font" style="font-size: 15px;font-weight:600;color:#5a5c69!important;">패스워드 확인</span>
											<input type="password" class="form-control form-control-user"
												id="userConfirmPasswd" name="userConfirmPasswd" placeholder="패스워드 확인" required> 
												
												<span id="valid_userConfirmPwd" style="color:red; font-size: small">${valid_userConfirmPwd}</span>
										</div>


								<br>


							
									<div class="d-grid gap-3 d-sm-flex justify-content-sm-center justify-content-xl-start">
										<%-- <select class="form-select" aria-label="Default select example" id="selectCategory" name="categoryNo" required="true">
											<option selected value="" label="전체보기"></option>
											<c:forEach items="${categoryList}" var="categoryList">
											<option value="${categoryList.categoryNo}">${categoryList.categoryName}</option>
											</c:forEach>
										</select> --%>
										<%-- <div>
										  <input class="form-check-input ml-0" type="checkbox" id="checkboxNoLabel" value="" aria-label="..." disabled="disabled" required>
										</div>&nbsp;
										<%@ include file="category/categorySelect.jsp"%> --%>
										
									</div>
								
										
										<span style="color:red; font-size: small"><c:out value="${valid_categoryNo}"></c:out></span>
										<hr><br>
										<!-- <a th:href="@{mainPage}"
									style="width: 355px; height: 50.13px;"
									class="btn btn-primary btn-user btn-block" onclick="joinClick()" > 회원가입 </a> -->
									<!--  style="width: 350px; height: 50.13px;" -->
									<input
									class="btn btn-primary btn-user btn-block" type="submit" id="insertUser" value="회원가입" name="insertUser">
									<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
									<a class="btn btn-secondary btn-user btn-block mt-3" id="BackLoginClick">돌아가기 </a>
								</form>
							 <br> 
							</div>
							
								<hr>
								


							</div>
						</div>
					</div>
				</div>
			</div>

		</div>
	

	

	

	<!-- Bootstrap core JavaScript-->
	<script src="vendor/jquery/jquery.min.js"></script>
	<script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

	<!-- Core plugin JavaScript-->
	<script src="vendor/jquery-easing/jquery.easing.min.js"></script>

	<!-- Custom scripts for all pages-->
	<script src="js/sb-admin-2.min.js"></script>

</body>
<script>

	
	//var flag = true;
	//커서가 빠져나가면 동작하는 메소드로 변경하기(focusout) => 키보드 입력감지 함수(keyup)
	//패스워드를 비교하는 함수야 당장 비동기(Ajax)로 처리하지 않고 밑에 스크립트단에 비교분석해서 결과만 돌려주면 되지만
	//앞으로 비동기로 아이디, 패스워드, ... 등등 유효성을 검사하는 모든 인풋박스에 대해 적용시켜야 하기 때문에 
	//ajax로 적용시키고자 한다. [2021.11.11 김민정]
	
	$(document).ready(function(){
		

		
		$("#userId").blur(function(){												//유저 아이디 중복성검사
			
			var flag;
		
			console.log("userid클릭");

			var token = $("meta[name='_csrf']").attr("content");
			var header = $("meta[name='_csrf_header']").attr("content");

			console.log("token+header : " + token + ", " + header);						//토큰확인

			$(document).ajaxSend(function(e, xhr, options) {
				xhr.setRequestHeader(header, token);
			});
 
			console.log("userId찍기 직전");
			
			var userId = $("#userId").val();
			//var Id_check=/([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/.test(userId);

			//console.log("아이디 유효성 검사 결과 :"+Id_check);
			
			console.log(userId);
			
			
			
			//유저아이디 유효성 체크
			var paramData = {			//hashMap과 유사하다,,, key, value값을 가진 형태 => JSON
				"userId" : userId
			};
			
			
			console.log("회원가입시 유저의 아이디? :"+JSON.stringify(paramData));
			
			
			
			
			
			$.ajax({
				contentType : "json",
				type : "post",
				url : "${pageContext.request.contextPath}/checkvalidid", 				// 사용자가 패스워드를 입력하여 보는 창은 join이 아니라 joinform이다.
																						//url은 controller에서의 매칭주소
				datatype : "json",
				contentType : "application/json",
				data : JSON.stringify(paramData),		//controller단에서 그냥 데이터를 보내면 못알아듣기 때문에 stringify로 형변환해서 보낸다.(결국에 JSON타입)
				beforeSend : function(xhr) { /*데이터를 전송하기 전에 헤더에 csrf값을 설정한다*/
					xhr.setRequestHeader(header, token);
				},
				
				cache : false,
				
				success : function(data){
					
					console.log(data);
					
					if(data == true){

						flag = true;		//유효성 체크가 되는경우 
					}
					else{

						flag = false;		//유효성 체크가 안되는 경우
					}
					
				},
				error:function(){
					alert("통신오류 입니다.");
				}
			});
					
			
			
			console.log("유효성 flag : "+flag);
			
			//유저아이디 중복성 체크
			$.ajax({
				contentType : "json",
				type : "post",
				url : "${pageContext.request.contextPath}/userIdCheck", 				// 사용자가 패스워드를 입력하여 보는 창은 join이 아니라 joinform이다.
																						//url은 controller에서의 매칭주소
				datatype : "json",
				contentType : "application/json",
				data : JSON.stringify(paramData),		//controller단에서 그냥 데이터를 보내면 못알아듣기 때문에 stringify로 형변환해서 보낸다.(결국에 JSON타입)
				beforeSend : function(xhr) { /*데이터를 전송하기 전에 헤더에 csrf값을 설정한다*/
					xhr.setRequestHeader(header, token);
				},
				
				cache : false,
				
				success : function(data){
					
					console.log(data);
					
					if(flag == true && data == 0){									//유효성 검사와 중복성 검사가 통과한경우
						$("#idNullCheck").text("사용할 수 있는 아이디입니다.");
						$("#idNullCheck").css('color','blue'); 
						$("#insertUser").attr("disabled",false);
						
					}
					
					else if(flag == false && data == 0){
						$("#idNullCheck").text("아이디는 이메일 형식으로 입력하셔야 합니다.");			//유효성 검사를 통과하지 않고 데이터 중복성이 통과한경우
						$("#idNullCheck").css('color','red'); 
						$("#userId").text("");
						$("#insertUser").attr("disabled",true);
						}
						
					else if(flag == true && data != 0)	{							//유효성 검사는 통과했지만 중복된 사용자인경우
						
						$("#idNullCheck").text('이미 있는 사용자입니다');
						$("#idNullCheck").css('color', 'red');	
						$("#userId").text("");
						$("#insertUser").attr("disabled",true);
					}
				
					else{																//유효성 검사와 중복성 검사에서 통과하지 못한 경우
						
						$("#idNullCheck").text('이미 있는 사용자입니다');
						$("#idNullCheck").css('color', 'red');	
						$("#userId").text("");
						$("#insertUser").attr("disabled",true);
					}
						
						
				},
				error:function(){
				
				alert("아이디 중복찾기 에러");
						flag = false;
						}
					});		//ajax
				
				
			});
		

		//비밀번호 검사
		//첫번째 비밀번호 유효성 확인 -> 통과시 비밀번호 확인 유효성 검사 -> 비밀번호 확인 유효성 통과시 -> 비밀번호와 비밀번호 확인 값 비교 -> 비교 통과시 disable false
/* 
			var	flag = false;
			var flag2 = false;
			var flag3 = false; */
			//var userPwd = document.getElementById("userPwd"); 
			
			//var userConfirmPasswd = document.getElementById("userConfirmPasswd"); 

			
			$("#userPwd").blur(function(){
				
				console.log("유저패스워드확인");
				
				$("#valid_userConfirmPwd").text("");		//텍스트를 공란으로 설정
				
				var token = $("meta[name='_csrf']").attr("content");
				var header = $("meta[name='_csrf_header']").attr("content");

				console.log("token+header : " + token + ", " + header);						//토큰확인

				$(document).ajaxSend(function(e, xhr, options) {
					xhr.setRequestHeader(header, token);
				});
				
				//flag2 = false;
				var userPwd = $("#userPwd").val();
				//var check = /^(?=.*[a-zA-Z]).{4,20}$/.test(userPwd);  //영문,특수문자 리턴값 true,false

				var paramData = {			//hashMap과 유사하다,,, key, value값을 가진 형태 => JSON
					"userPwd" : userPwd
				};
			
			
				console.log("회원가입시 유저의 아이디? :"+JSON.stringify(paramData));
				
				$.ajax({
				contentType : "json",
				type : "post",
				url : "${pageContext.request.contextPath}/checkvalidpwd", 				// 사용자가 패스워드를 입력하여 보는 창은 join이 아니라 joinform이다.
																						//url은 controller에서의 매칭주소
				datatype : "json",
				contentType : "application/json",
				data : JSON.stringify(paramData),		//controller단에서 그냥 데이터를 보내면 못알아듣기 때문에 stringify로 형변환해서 보낸다.(결국에 JSON타입)
				beforeSend : function(xhr) { /*데이터를 전송하기 전에 헤더에 csrf값을 설정한다*/
					xhr.setRequestHeader(header, token);
				},
				
				cache : false,
				
				success : function(data){
					
					console.log("패스워드 유효성 검사 결과 : "+data);
					
					if(data == true){													

						console.log("패스워드 유효성 통과");										//비밀번호 유효성 체크가 되는 경우			
						
						$("#validPwd").text("사용할 수 있는 조합입니다.");
						$("#validPwd").css('color','blue');
						$("#insertUser").attr("disabled",false);
						
						
					}
					else{																//비밀번호 유효성 체크가 안되는 경우

						console.log("패스워드 유효성 통과못함");
						
						$("#validPwd").text("사용할 수 없는 조합입니다.\n패스워드 설정안내를 확인해 주세요.");
						$("#validPwd").css('color','red');
						$('#userPwd').val('');							//비밀번호 값(인풋태그) 공란으로 하기 
						$("#insertUser").attr("disabled",true);
						
					}
					
				},
				error:function(){
					alert("통신오류 입니다.");
				}
			});

				
				/* if(check == false){									//비밀번호 유효성 통과가 안될때
					
					console.log("패스워드 유효성 불통과 진입");
					//alert("사용할 수 없는 조합입니다.\n패스워드 설정안내를 확인해 주세요.");
					
					$("#validPwd").text("사용할 수 없는 조합입니다.\n패스워드 설정안내를 확인해 주세요.");
					$("#validPwd").css('color','red');
					$('#userPwd').val('');							//비밀번호 값(인풋태그) 공란으로 하기 
					
					$("#insertUser").attr("disabled",true);
					flag2 = false;
					
				}	
				else{										//비밀번호란의 유효성검사에서 통과될때
					console.log("패스워드 일치진입");
					flag2 = true;
					//alert("사용할 수 있는 조합입니다.")
					$("#validPwd").text("사용할 수 있는 조합입니다.");
					$("#validPwd").css('color','blue');
					$("#insertUser").attr("disabled",true);
				}
				
				console.log("flag2 : "+ flag2);
				return flag2; */				
				
			});
			
			
	
		
			$("#userConfirmPasswd").blur(function(){
				
				var userPwd = $("#userPwd").val();
				var userConfirmPasswd = $("#userConfirmPasswd").val();
				
				if(userPwd != ""){
					
					if(userPwd == userConfirmPasswd){
						$("#valid_userConfirmPwd").text("패스워드가 일치합니다.");
						$("#valid_userConfirmPwd").css('color','blue');
						$("#insertUser").attr("disabled",false);
					}else{
						$("#valid_userConfirmPwd").text("패스워드가 불일치합니다.");
						$("#valid_userConfirmPwd").css('color','red');
						$('#userPwd').val('');	
						$("#userConfirmPasswd").val('');
						$("#validPwd").text("");
						$("#insertUser").attr("disabled",true);
					}
					
				}else{
					$("#valid_userConfirmPwd").text("패스워드를 입력해주세요.");
					$("#valid_userConfirmPwd").css('color','red');
					$("#insertUser").attr("disabled",true);
					$("#userPwd").focus();
				}
				
				
			});
			
		
			/* $("#insertUser").click(function(){
				
				if($("input:checkbox[id='checkboxNoLabel']").is(":checked") == false){
					
					Swal.fire({
						  icon: 'error',
						  title: '관심사를 선택해야합니다.',
					});
					
					return false;
				}
				
				var x = document.getElementById("userInputForm").submit();
				
				
			}); */
			
	
			
	});
	
	
	$("#BackLoginClick").click(function(){
		
		history.back();
		
		
	});
	
	
	
	
	
</script>

</html>