<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>



<!--  S : CSS -->
<!-- <link href="css/detail/detailPage.css" rel="stylesheet" /> -->
<link href="css/detail/grade.css" rel="stylesheet">
<!--  E : CSS -->
</head>

<body>
	<div class="star-ratings" style="font-size: -webkit-xxx-large;">
		<div class="star-ratings-fill space-x-2 text-lg"  id="divBGrade">
			<span class="starIcon">★</span><span>★</span><span>★</span><span>★</span><span>★</span>
		</div>
		<div class="star-ratings-base space-x-2 text-lg">
			<span>★</span><span>★</span><span>★</span><span>★</span><span>★</span>
		</div>
	</div>
</body>

<!--  S : JS -->
<!-- <script src="js/detailPage.js"></script> -->
<!--  E : CSS -->
<script>	
		$("#divBGrade").attr('style',"width : "+${study.getBGrade()}+"%");
</script>


</html>