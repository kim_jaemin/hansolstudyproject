<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!-- S : HEADER -->  
<%@ include file="../common/header.jsp" %>
<!-- E : HEADER -->  

<!-- S : NAVBAR  -->
<!-- <link href="/css/common/navbar.css" rel="stylesheet"> -->
<!-- E : NAVBAR  -->
<script src="/js/common/navbar.js"></script>

</head>

<!-- <script src="js/common/navbar.js"></script>
 -->


<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
		<div id="toastMessage" class="position-fixed top-0 end-0 p-3" style="z-index: 5">
			</div>
		<div class="container px-4 px-lg-5">
			<a class="navbar-brand" href="/mainPage" style="font-weight:800;">STUDYMOA</a>
			<button class="navbar-toggler" type="button"
				data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent"
				aria-controls="navbarSupportedContent" aria-expanded="false"
				aria-label="Toggle navigation">
				<span class="navbar-toggler-icon"></span>
			</button>
			<div class="collapse navbar-collapse" id="navbarSupportedContent">
				<ul class="navbar-nav me-auto mb-2 mb-lg-0 ms-lg-4">
					<c:if test='${sessionScope.SPRING_SECURITY_CONTEXT.authentication.principal.status==1}'>
					<li class="nav-item"><a class="nav-link" href="/adminPage">관리자페이지</a></li>
					</c:if>
					<li class="nav-item"><a class="nav-link"
						aria-current="page" href="/mainPage">스터디보기</a></li>
					<li class="nav-item"><a class="nav-link" href="/myPage">마이페이지</a></li>
					<li class="nav-item"><a class="nav-link" href="/makeStudy">스터디만들기</a></li>
				</ul>
				<!-- <a class="btn btn-outline-info mr-3"
										data-bs-toggle="modal" data-bs-target="#exampleModal"
										id="endStudy" style="color:white;">프로필 사진 추가</a> -->							
				
				<button class="btn mr-3"
										data-bs-toggle="modal" data-bs-target="#exampleModal"
										id="endStudy" style="color:white;">프로필 사진 추가</button>
				
				<form class="profileButton my-1" action="/logOut" >
					<button class="btn btn-outline-light" type="submit">
						<img src="${sessionScope.SPRING_SECURITY_CONTEXT.authentication.principal.userProfileImg}" style="width:20px; height:23px"> <sec:authentication property="principal.userName"/><span
							class="badge bg-dark text-white ms-1 rounded-pill">logOut</span>
					</button>
				</form>
			</div>
			
		</div>
	</nav>		
	
	<div class="modal fade" id="exampleModal" tabindex="-1"
		aria-labelledby="exampleModalLabel" aria-hidden="true">
		<div
			class="modal-dialog modal-dialog-centered modal-dialog-scrollable">
			<div class="modal-content">
				<form
					action="./insertProfileImg?${_csrf.parameterName}=${_csrf.token}"
					method="POST" enctype="multipart/form-data" name="readForm"
					autocomplete="off">
					<div class="modal-header">
						<h5 class="modal-title" id="exampleModalLabel">프로필 사진 추가</h5>
					</div>
					<div class="modal-body">
						<div class="inputArea">
							<label for="gdsImg">이미지</label> <input type="file" id="gdsImg"
								name="file" accept=".png, .jpeg, .jpg" />
							<div class="select_img">
								<img src="" />
							</div>
						</div>

						<div class="inputArea">
							<!-- <button type="submit" id="register_Btn" class="btn btn-primary">등록</button> -->
						</div>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-secondary"
							data-bs-dismiss="modal">닫기</button>
						<button type="submit" class="btn btn-primary"
							id="register_Btn">등록</button>
					</div>
				</form>
			</div>
		</div>
	</div>
