<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="en">
<!-- 로그인 페이지를 구현하기 위해 대시보드에서 제공하는 login.html이다. 김민정 -->


<head>


    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    
    <meta name="_csrf" id="_csrf" content="${_csrf.token}" />
	<meta name="_csrf_header" id="_csrf_header"
	content="${_csrf.headerName}" />
    

    <title>StudyMoa Find ID</title>
<!-- 제이쿼리를 쓰기위한 스크립트 -->

	<script src="https://code.jquery.com/jquery-3.6.0.js"
		integrity="sha256-H+K7U5CnXl1h5ywQfKtSj8PCmoN9aaq30gDh27Xc0jk="
		crossorigin="anonymous"></script>

<!-- sweet alert -->
<script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>

    <!-- Custom fonts for this template-->
    <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
    <link
        href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i"
        rel="stylesheet">

    <!-- Custom styles for this template-->
    <link href="css/styles.css" rel="stylesheet" />
    <link href="css/sb-admin-2.css" rel="stylesheet">
	<link href="css/common.css" rel="stylesheet">
	
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
	<%-- <%@ include file="common/header.jsp"%> --%>
</head>

<body class="bg-dark">

    <div class="container">

        <!-- Outer Row -->
        <div class="row justify-content-center">

            <div class="col-xl-10 col-lg-12 col-md-9">

                <div class="card o-hidden border-0 shadow-lg my-5" style="border:solid 0px !important;padding:0px !important;">
                    <div class="card-body p-0">
                        <!-- Nested Row within Card Body -->
                        <div class="row" style="padding-bottom:0px !important;">
                            <div class="loginImage col-lg-6 d-none d-lg-block"></div>
                            <div class="col-lg-6">
                                <div class="p-5">
                                    <div class="text-center">
                                        <h1 class="h4 text-gray-900 mb-4">아이디찾기</h1> <!-- 제목 -->
                                    </div>
                                    
                                    
                                   <form class="user" action="${pageContext.request.contextPath}/findidForm" id="userFindIdForm" name="userFindIdForm" method="post">
                                        
                                        
                                        <div class="form-group">
                                           
                                           <span class="jg-font" style="font-size: 15px;font-weight:600;color:#5a5c69!important;">이름</span>
                                            <input type="text" class="form-control form-control-user"
                                                id="userName" name="userName" aria-describedby="emailHelp"
                                                placeholder="이름">
                                        </div>
                                        
                                        
                                        <div class="form-group">
                                        <span class="jg-font" style="font-size: 15px;font-weight:600;color:#5a5c69!important;">전화번호</span>
                                            <input type="tel" class="form-control form-control-user"
                                                id="userPhone" name="userPhone" placeholder="전화번호">
                                        </div>
                                        
                                        
                                      	<br><hr><br>
                                       <input class="btn btn-primary btn-user btn-block" type="submit" id="findIdBtn" value="아이디찾기" name="findIdBtn">
									<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
                                        
                                        <br>
                                       <a class="btn btn-primary btn-user btn-block" id="BackLoginClick">
                                            돌아가기 </a>
                                        
                                        
		                                        <script type="text/javascript">
															
															console.log("${userId}");
		
															
															
														
															
												</script>
                                        
                                    </form>
                                    
                                    
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

        </div>

    </div>

    <!-- Bootstrap core JavaScript-->
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

    <!-- Core plugin JavaScript-->
    <script src="vendor/jquery-easing/jquery.easing.min.js"></script>


</body>
<script>

$(document).ready(function(){

	var token = $("meta[name='_csrf']").attr("content");
	var header = $("meta[name='_csrf_header']").attr("content");
	
	console.log("token+header : " + token + ", " + header);
	
	$(document).ajaxSend(function(e, xhr, options) {
		xhr.setRequestHeader(header, token);
	});

	
	$("#findIdBtn").attr("disabled",true);
	//userName -> userPhone
	$("#userName").blur(function(){
		
		var userName = $("#userName").val();
		
		if(userName == ""){											//이름이 빈값이면
			console.log("이름이 빈값이다");
			alert("이름을 확인해주세요");
			$("#findIdBtn").attr("disabled",true);
		}else{															//이름이 빈값이 아니면
			$("#userPhone").blur(function(){								//폰 값 확인
				var userPhone =  $("#userPhone").val();
				
				if(userPhone == ""){
					alert("전화번호를 확인해주세요");
					$("#findIdBtn").attr("disabled",true);
				}else{
				$("#findIdBtn").attr("disabled",false);					//이름과 폰 값이 null이 아닌경우
				}
				
			});
		}
		
		
		
	});

	$("#findIdBtn").click(function(){
		
		document.querySelector("#userFindIdForm").addEventListener('submit', function(e) {
	        e.preventDefault();

	        
	    });
		
			//var x = document.getElementById("userFindIdForm").submit();
		
			var userName = $("#userName").val();
			var userPhone = $("#userPhone").val();
		

			var paramData = {"userName" : userName,
							 "userPhone" : userPhone};
			
			console.log(JSON.stringify(paramData));
			
			/* if(userName == null && userPhone == null){
				alert("이름과 전화번호를 입력해주세요");
			}
			 */
		$.ajax({
			
			url:"${pageContext.request.contextPath}/findidForm"
		   ,type:"post"
		   ,dataType:"json"
		   ,contentType: 'application/json'
		   ,data:JSON.stringify(paramData)
		   ,beforeSend : function(xhr) { /*데이터를 전송하기 전에 헤더에 csrf값을 설정한다*/
				xhr.setRequestHeader(header, token);
			}
		   ,async : false
		   ,cache : false
		   ,success:function(data){
			   if(data.USER_ID != null){
				   Swal.fire({
					   title: "아이디를 찾았습니다!",
					   text: data.USER_ID+" 를 찾았습니다.",
					   icon: "info",																	//버튼 종류는 warning, success, info, "error"가 있다.
					   buttons: true,
					   dangerMode: true,
					 })
					 .then((willDelete) => {
					   if (!willDelete) {
					     swal("계속 로그인을 진행해주세요", {
					       icon: "success",
					     });
					   } 
					 });
				   
				  
				   

				   
			   /* alert(data.USER_ID+"를 찾았습니다."); */
			   }else{
				   Swal.fire({
					   title: "아이디를 찾지 못했습니다!",
					   text: "이름과 핸드폰 번호를 다시 체크해주시기 바랍니다.",
					   icon: "warning",
					   buttons: "아이디 다시찾기",
					  
					 });
			   }
		   },
		   error : function(){
			   alert("통신실패");
		   }
				
			
			
			
			
			
			
		});
		
		
	});


		$("#BackLoginClick").click(function(){
		
		history.back();
		
		
	});
	
});
	
</script>
</html>