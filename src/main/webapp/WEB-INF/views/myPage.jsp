<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html>

<head>
	<!-- Core theme JS-->
	<script src="js/scripts.js"></script>
	<link href="css/styles.css" rel="stylesheet">
	<link href="css/common.css" rel="stylesheet">
	<link href="css/myPage.css" rel="stylesheet">
	<!-- Core theme CSS (includes Bootstrap)-->
</head>

<body class="d-flex flex-column h-100">
	
	<%@ include file="common/navbar.jsp"%>
	
	<main class="flex-shrink-0">
	
	<!-- Header -->
	<div class="mainbar bg-dark py-3" style="background: url(/img/visual2.png);background-color:#deebe9">
		<div class="container px-5" style="padding-left: 600px !important;max-width: 1600px;">
			<div class="row gx-5 align-items-center justify-content-center">
				<div class="col-xl-8 col-xl-7 col-xxl-6">
					<div class="title my-5 text-center text-xl-start" style="margin-top: 180px !important;">
						<h1 class="display-15 fw-bolder mb-2">스터디의 모든 것을 관리하세요</h1>
					<div class="d-grid gap-3 d-sm-flex justify-content-sm-center justify-content-xl-start"></div>
					</div>
				</div>
				
			</div>
		</div>
	</div>
	
	<!--  include문으로 full calendar 붙이기 -->
<%-- 	<%@ include file="fullcalendar2.jsp" %> --%>
	<!--  달력 소환 -->
	<!-- Blog preview section-->
	<section class="">
		<!-- Contact form-->
		<div class="container px-5">
		<!-- Contact form-->
		
		<div class="rounded-3 py-5 px-4 px-md-5 mb-5">  <!-- 회색배경 -->
			<div class="text-center mb-5">
		
		
		
		
		
		<div class="btn-group mb-3">
		
		
				<!--  가입한 스터디 보기 -->	
				<a class="fs-5 px-2 link" data-toggle="tooltip" data-placement="top" title="내 스터디">
									<div class="feature bg-secondary bg-gradient text-white rounded-3 mb-3">
			<i class="bi bi-journal-bookmark-fill"></i>
					</div>				

</a>	


	
		<!--  회원정보수정 -->
		<a class="fs-5 px-2 link" href="/useredit" data-toggle="tooltip" data-placement="top" title="회원정보 수정" >
					<div class="feature bg-dark bg-gradient text-white rounded-3 mb-3">
				<i class="bi bi-person-fill" ></i>
				</div>
					</a>			
					
				<!--  캘린더 -->
				
	<a class="fs-5 px-2 link-dark" href="/showCalendar" data-toggle="tooltip" data-placement="top" title="캘린더">
					<div class="feature bg-dark bg-gradient text-white rounded-3 mb-3">
					<i class="bi bi-calendar-check-fill"></i>
					</div>	
			</a>			
					
					<!--  새로운 스터디 만들기 -->
					
				<a class="fs-5 px-2 link-dark" href="/makeStudy" data-toggle="tooltip" data-placement="top" title="스터디 만들기">
								<div class="feature bg-dark bg-gradient text-white rounded-3 mb-3">
					<i class="bi bi-journal-plus"></i>
					</div>	
		</a>
	
	</div>

				
					<h1 class="fw-bolder">내 스터디</h1>
							<strong><sec:authentication property="principal.userName"/></strong>님의 스터디는 <strong>${mystudySize}</strong>개 입니다</p>
				
				</div>
	
	<div class="o-hidden border-0 my-5">
		<!-- 전체 스터디 조회(참고용) -->
		
		<!--  card header -->
		<div class="bg-white rounded-3 px-4 px-md-5 mb-5">
		
		<!--  가입한 스터디가 없을 때 보이는 화면 -->
		<div class="body p-4">
			<c:choose>
				<c:when test="${mystudySize == 0}">
					<div class="row justify-content-center">
                        <div class="col-lg-8 col-xxl-6">
                            <div class="text-center my-3">
                                <h1 class="fw-bolder mb-3">아직 <br> 가입한 스터디가 없네요!</h1>
                                <p class="lead fw-normal text-muted mb-4">새로운 스터디를 만들어 스터디장이 되어보세요<br> 관심있는 분야의 스터디에 가입해보세요 <br> 다양한 사람들과 함께하는 멋진 스터디가 될 거에요</p>
                                
                          
                                <a type="button" class="btn btn-outline-secondary" href="/mainPage">다른 스터디 찾아보기</a>
                               
                                 <a type="button" class="btn btn-outline-dark" href="/makeStudy">새로운 스터디 만들기</a>
                            </div>
                        </div>
                    </div>
				</c:when>
				<c:otherwise>
				</c:otherwise>
			</c:choose>
						
							</div>
		
		<!-- 아래에 이게 카드 3개 정렬해주는 구문이었음  -->
	<section class="sectionBody ">
	 <div class="container px-5 ">	
	  <div class="row gx-5">
		<c:forEach var="list" items="${list}">
			<div class="col-lg-4 mb-5" id="goDetailStudy">
				<div class="card h-100 shadow border-0">

					  
					<input type="hidden" name="smNo" id="studySmNo" value="${list.SM_NO}"> <input type="hidden" name="sNo" id="studySNo" value="${list.S_NO}"> <img class="card-img-top justify-content-center " src="${list.CATEGORY_THUMBIMG}" alt="..." />


<!-- card body -->

					<div class="card-body p-4">
						<c:choose>
							<c:when test="${list.SM_ROLE == '스터디장'}">
								<div class="badge bg-primary bg-gradient rounded-pill mb-2">스터디장</div>
							</c:when>
						<c:otherwise>
								<div class="badge bg-primary bg-gradient rounded-pill mb-2">스터디원</div>
						</c:otherwise>
						</c:choose>
						<a class="text-decoration-none link-dark stretched-link" href="${pageContext.request.contextPath}/detailPage?sNo=${list.S_NO}&smNo=${list.SM_NO}">

							<h5 class="card-title mb-3">${list.S_TITLE}</h5>
						</a>
						<p class="card-text mb-0">${list.S_CONTENT}</p>

					</div>



					<div class="card-footer p-4 pt-0 bg-transparent border-top-0">
						<div class="d-flex align-items-end justify-content-between">
							<div class="d-flex align-items-center">
								<img class="rounded-circle me-3" src="/img/king.png" alt="..." />
								<div class="small">
									<div class="fw-bold">${list.USER_NAME}</div>
									<div class="text-muted">${list.S_WRITE_DATE}&middot;현재인원 : ${list.S_NOW} / ${list.S_TOTAL}</div>
								</div>
							</div>
					
						</div>
					</div>
				</div>

			</div>
		</c:forEach>
		
	  </div>
	 </div> 
	</section>  
	

	
	<!-- 페이징 include 처리 -->
					<%@ include file="mypage/myPagination.jsp"%>

	</div>
	</div>
	</div>
	</div>



		</section>
	</main>




		<!-- Footer-->
		<%@ include file="common/footer.jsp" %>
		
	</body>
		
	<script src="js/myPage.js"></script>
	
		<script>
	$(function () {
		  $('[data-toggle="tooltip"]').tooltip()
		})
	
	</script>
	

	
</html>