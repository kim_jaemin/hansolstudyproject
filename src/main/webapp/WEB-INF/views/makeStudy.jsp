<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page import="java.io.PrintWriter"%>

<!DOCTYPE html>
<html>
<head>

<link rel="stylesheet" href="css/datepicker.css">
<link rel="stylesheet" href="css/datepickermain.css">
<link href="css/icons.css" rel="stylesheet">
<link href="css/styles.css" rel="stylesheet">
<link href="css/common.css" rel="stylesheet">
<link href="css/makeStudy.css" rel="stylesheet">

</head>

<body class="d-flex flex-column h-100">
<%@ include file="common/navbar.jsp"%>

<main class="flex-shrink-0">
	
	<!-- Header -->
	<div class="mainbar bg-dark py-3" style="background: url(/img/visual3.png);background-color:#deebe9">
		<div class="container px-5" style="padding-left: 600px !important;max-width: 1600px;">
			<div class="row gx-5 align-items-center justify-content-center">
				<div class="col-xl-8 col-xl-7 col-xxl-6">
					<div class="title my-5 text-center text-xl-start" style="margin-top: 180px !important;">
						<h1 class="display-15 fw-bolder mb-2">스터디를 만들어보세요</h1>
					<div class="d-grid gap-3 d-sm-flex justify-content-sm-center justify-content-xl-start"></div>
					</div>
				</div>
				
			</div>
		</div>
	</div>
	
	<!-- Blog preview section-->
	<section class="">
		<!-- Contact form-->
		<div class="container px-5">
			<!-- Contact form-->
			<div class="rounded-3 py-5 px-4 px-md-5 mb-5">  <!-- 회색배경 -->
				<div class="text-center mb-5">
					<div class="btn-group mb-3">
						<!--  가입한 스터디 보기 -->	
						<a class="fs-5 px-2 link"  href="/myPage" data-toggle="tooltip" data-placement="top" title="내 스터디">
							<div class="feature bg-dark bg-gradient text-white rounded-3 mb-3">
								<i class="bi bi-journal-bookmark-fill"></i>
							</div>
						</a>
						<!--  회원정보수정 -->
						<a class="fs-5 px-2 link" href="/useredit" data-toggle="tooltip" data-placement="top" title="회원정보 수정" >
							<div class="feature bg-dark bg-gradient text-white rounded-3 mb-3">
								<i class="bi bi-person-fill" ></i>
							</div>
						</a>
						<!--  캘린더 -->
						<a class="fs-5 px-2 link-dark" href="/showCalendar"  data-toggle="tooltip" data-placement="top" title="캘린더">
							<div class="feature bg-dark bg-gradient text-white rounded-3 mb-3">
								<i class="bi bi-calendar-check-fill"></i>
							</div>	
						</a>			
						<!--  새로운 스터디 만들기 -->
						<a class="fs-5 px-2 link-dark"  data-toggle="tooltip" data-placement="top" title="스터디 만들기">
							<div class="feature bg-secondary bg-gradient text-white rounded-3 mb-3">
								<i class="bi bi-journal-plus"></i>
							</div>
						</a>
					</div>
					
					<h1 class="fw-bolder">스터디 만들기</h1>
					<p>일정을 관리할 수 있어요 </p>
				</div>
				
				<div class="card o-hidden border-0 shadow-lg my-5 mb-3"><br>
					<div class="container px-5">	
						<div class="row gx-5">
							<form class="makeStudy needs-validation" novalidate="novalidate" action="createStudy" method="get" name="createStudy" autocomplete="off" id="makeStudyForm">
								<!-- 01 스터디 제목 S_TITLE -->
								<div class="form-floating mb-2 label">스터디 제목</div>
								<div class="form-floating mb-3">
									<input type="text" class="form-control form-control-user"
										placeholder="스터디 제목을 입력해주세요."  name="sTitle"  data-toggle="tooltip" data-placement="bottom" title="만들고 싶은 스터디의 이름"
										required="required" minlength="4" maxlength="20"> 
<!-- 										<label for="name">스터디 제목</label> -->
									<div class="valid-feedback"><span class="badge badge-pill badge-success">SUCCESS</span></div>
									<div class="invalid-feedback"><span class="badge badge-pill badge-danger">DISABLED</span>스터디 제목은 4-20자로 입력해야 합니다.</div>
								</div>
								
								<div class="separator"></div>
								
								<div class="container">
									<div class="row gx-5">
										<div class="d-grid gap-3 d-sm-flex justify-content-sm-center justify-content-xl-start">
											<div>
												<input class=" ml-0" type="checkbox" 
												id="checkboxNoLabel" value="" aria-label="..." 
												disabled="disabled" required>
											</div>
											&nbsp;
										<%@ include file="category/categorySelect.jsp"%>
										
										</div>
									</div>
								</div>
								
								<div class="separator"></div>
								
								<div class="group-date row">
									<!-- 02-1 모집 기간  -->
									<div class="col-md">
										<div class="form-floating mb-2 label">모집기간</div>
										<div class="input-group">
											<div class="npl col-md">
												<div class="form-floating">
													<input type="text" class="form-control form-control-user start-date" placeholder="Start date" id="start" name="recruitSdate" required="required">
<!-- 													<label for="start">Start Date</label> -->
													<div class="valid-feedback"><span class="badge badge-pill badge-success">SUCCESS</span></div>
													<div class="invalid-feedback"><span class="badge badge-pill badge-danger">DISABLED</span></div>
												</div>
											</div>
											~
											<div class="npr col-md">
												<div class="form-floating">
													<input type="text" class="form-control form-control-user end-date" placeholder="End date" id="end" name="recruitEdate" required="required"> 
<!-- 													<label for="end">End Date</label> -->
													<div class="valid-feedback"><span class="badge badge-pill badge-success">SUCCESS</span></div>
													<div class="invalid-feedback"><span class="badge badge-pill badge-danger">DISABLED</span></div>
												</div>
											</div>
										</div>
									</div>
								</div>
								
								<div class="separator"></div>
								
								<div class="group-date row">
									<!--02-2 모임 기간 -->
									<div class="col-md">
										<div class="form-floating mb-2 label">모임기간</div>
										<div class="input-group">
											<div class="npl col-md">
												<div class="form-floating">
													<input type="text" placeholder="Start date" name="classSdate" aria-label="First name" class="form-control form-control-user start-date" required="required">
<!-- 													<label for="content">Start Date</label> -->
													<div class="valid-feedback"><span class="badge badge-pill badge-success">SUCCESS</span></div>
													<div class="invalid-feedback"><span class="badge badge-pill badge-danger">DISABLED</span></div>
												</div>
											</div>
											~
											<div class="npr col-md">
												<div class="form-floating">
													<input type="text" placeholder="End date" aria-label="Last name" name="classEdate" class="form-control form-control-user end-date" required="required"> 
<!-- 													<label for="content">End Date</label> -->
													<div class="valid-feedback"><span class="badge badge-pill badge-success">SUCCESS</span></div>
													<div class="invalid-feedback"><span class="badge badge-pill badge-danger">DISABLED</span></div>
												</div>
											</div>
										</div>
									</div>
									
								</div>
								
								<div class="separator"></div>
								
								<!-- 02-3  모임 장소 -->
								<div class="form-floating mb-2 label">장소</div>
								<div class="form-floating mb-3">
									<input type="text" class="form-control form-control-user" id="content"
										name="classLocation" placeholder="스터디를 진행할 장소를 적어주세요. ex) 홍대입구역 " data-toggle="tooltip" data-placement="bottom" title="스터디를 진행할 장소입니다. ex) 홍대입구역"
										required="required" >
		<!-- 								<label for="content">장소 </label> -->
									<div class="valid-feedback"><span class="badge badge-pill badge-success">SUCCESS</span></div>
									<div class="invalid-feedback"><span class="badge badge-pill badge-danger">DISABLED</span>진행 장소를 정해주세요</div>
								</div>
								
								<div class="separator"></div>
								
								<!--  02-4 모임 시간 -->
								<div class="form-floating mb-2 label">시간</div>
								<div class="form-floating mb-3">
									<input type="text" class="form-control form-control-user" id="sContent"
										name="classTime" required="required"
										placeholder="스터디를 진행할 시간을 적어주세요. ex) 11:00 ~ 17:00 " data-toggle="tooltip" data-placement="bottom" title="스터디를 진행할 시간입니다 ex) 11:00 ~ 17:00 or 미정 ... " > 
		<!-- 								<label for="sContent">시간</label> -->
									<div class="valid-feedback"><span class="badge badge-pill badge-success">SUCCESS</span></div>
									<div class="invalid-feedback"><span class="badge badge-pill badge-danger">DISABLED</span>시간을 정해주세요</div>
								</div>
								
								<div class="separator"></div>
								
								<!-- 02-5 스터디 내용 -->
								<div class="form-floating mb-2 label">스터디 내용</div>
								<div class="form-floating mb-3">
									<textarea class="form-control form-control-user-ta" placeholder="스터디 내용을 입력해주세요" data-toggle="tooltip" data-placement="bottom" title="⠀⠀⠀스터디 내용입니다⠀⠀⠀⠀ ⠀ex) 목적, 목표, 진행 계획 ... "
										style="height: 300px" id="sContent" name="sContent" required="required"
										minlength="10"></textarea>
		<!-- 						<label for="sContent" font >스터디 내용</label> -->
									<div class="valid-feedback"><span class="badge badge-pill badge-success">SUCCESS</span></div>
									<div class="invalid-feedback"><span class="badge badge-pill badge-danger">DISABLED</span>   스터디 내용은 10자 이상 입력해야 합니다.</div>
								</div>
								
								<div class="separator"></div>
								
								<div class="row">
									<div class="row-sm-2">
									<!--  04 참여 인원 S_TOTAL -->
										<div class="form-floating mb-2 label">참여 인원</div>
										<div class="form-floating mb-3">
											<input type="number" min="2" max="30" value="2" class="form-control form-control-user" placeholder="참여 인원은 총 몇 명인가요?" id="usr" name="sTotal">
											<div class="alertc" style="margin-top:5px;">최소 2명 - 최대 30명 입니다</div>
											<div class="valid-feedback"><span class="badge badge-pill badge-success"> SUCCESS </span></div>
											<div class="invalid-feedback"><span class="badge badge-pill badge-danger">DISABLED</span>   불가능한 인원입니다.</div>
										</div>
									</div>
								</div>
								
								<div class="row">
									<!-- 05 생성 전 확인 버튼 -->
									<div class="col-sm d-md-flex justify-content-md-end">
										<div class="form-check">
											<input class="form-check-input" type="checkbox" value=""
												id="invalidCheck" required> 
												<div style="margin-top: 8px;">
													<label class="jg-font label" for="invalidCheck" style="font-size : 17px;">스터디장이 되는 것에 동의합니다</label>
												</div>
											<p class="jg-font"><div class="invalid-feedback"><span class="badge badge-pill badge-danger">DISABLED</span>        반드시 동의해야 생성이 완료됩니다.</div></p>
										</div>
									</div>
								</div>
								
					<!-- 06 생성/취소 버튼 -->

<div class="d-grid gap-2 d-md-flex justify-content-md-end">

							<button type="submit" id="makeStudy"
								class="btn btn-dark text-white" >스터디 만들기</button>
							<button type="button" id="mainPage"
								class="btn btn-outline-secondary text-dark">취소</button>

</div>								



</form>
</div>							
</div>
</div>
</div>


</div>


	</section>
	</main>

	<!-- Footer-->
	<%@ include file="common/footer.jsp"%>
</body>

<!--  bootstrap 유효성검사를 위한 script -->
<script src="js/makeStudy/makeStudy.js"></script>

<!-- datepicker -->
<script src="js/datepicker.js"></script>



<script>
	$(function() {
		$('#sTitle').keyup(function(e) {
			var content = $(this).val();
			$(this).height(((content.split('\n').length + 1) * 1.5) + 'em');
			$('#counter').html(content.length + '/300');
		});
		$('#sTitle').keyup();
	});
</script>


	<script>
	$(function () {
		  $('[data-toggle="tooltip"]').tooltip()
		})
	
	</script>
	

</html>