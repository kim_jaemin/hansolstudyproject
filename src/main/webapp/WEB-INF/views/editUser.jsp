<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="EUC-KR"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<%@ include file="common/header.jsp" %>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    
    <meta name="_csrf" id="_csrf" content="${_csrf.token}" />
	<meta name="_csrf_header" id="_csrf_header"
	content="${_csrf.headerName}" />
    <link href="css/common.css" rel="stylesheet">

</head>
<body>

	<!-- Nested Row within Card Body -->
	<form class="user" action="/usereditForm" id="usereditForm" method="post" >
	<div class="group row" style="height: 300px;">
	<input type="hidden" name="userOrgPwd" value="${ sessionScope.SPRING_SECURITY_CONTEXT.authentication.principal.userPwd}" />
	<table class="editUserTable">
	 <tbody>
	  <tr>
	  	<th class="jg-fontTh" ><span class="badgeCommon badge-secondary">ID</span></th>
	  	<td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
	  	<td class="jg-fontTh" style="text-align: start;">${ sessionScope.SPRING_SECURITY_CONTEXT.authentication.principal.userId}</td>
	  	<td></td>
	  	<th ><span class="badgeCommon badge-secondary">이름</span></th>
	  	<td class="jg-fontTh "><input value="${sessionScope.SPRING_SECURITY_CONTEXT.authentication.principal.userName}" id="inputName" name="userName" disabled></td>
	  	<td><button type="button" class="modifyEditUser btn btn-light" id="userNameBtn">수정</button></td>
	  </tr>
	  <tr>
	  	<th><span class="badgeCommon badge-secondary" >전화번호</span></th>
	  	<td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
	  	<td class="jg-fontTh"><input value="${ sessionScope.SPRING_SECURITY_CONTEXT.authentication.principal.userPhone}" id="inputPhone" name="userPhone" disabled ></td>
	  	<td><button type="button" class="modifyEditUser btn btn-light mx-2" id="editPhoneBtn">수정</button></td>
	  	
	  	<th><span class="badgeCommon badge-secondary">비밀번호</span></th>
	  	<td class="jg-fontTh">
	  		<input type="text"  placeholder="변경할 비밀번호"  id="userChangePwd" name="userChangePwd" required>
	  		<input type="text"  placeholder="비밀번호 확인" id="userChangePwd2" name="userChangePwd2" style="margin-top: 10px !important;" required></td>
	  	<td></td>
	  </tr>
	 </tbody>
	</table>
	</div>
	
	

	<!--	<div class="form-group">
	<input type="text" class="form-control form-control-user"
	id="userinsertPwd" name="userinsertPwd" placeholder="기존 비밀번호">
	</div> -->
						
	<!-- <input type="text" class="form-control form-control-user" id="userChangePwd" name="userChangePwd" placeholder="변경할 비밀번호" required>
	<input type="text" class="form-control form-control-user" id="userChangePwd2" name="userChangePwd2" placeholder="비밀번호 확인" required> -->
							
				
					
	<div class="d-grid gap-2 d-md-flex justify-content-md-end">
																			
	<button type="submit" class="btn btn-dark text-white btn-user" id="editBtn" name="editBtn">회원정보수정</button>
										   
	<button type="button" class="btn btn-dark nav-link" id="deleteuser" name="deleteuser">회원 탈퇴</button>
	<input type="reset" value="취소" class="btn btn-outline-secondary text-dark btn-user" >
									
	</div>						
	</form>
									

</body>

<script>

	var flag1 = false;
	$("#editPhoneBtn").click(function(){
		
		console.log("수정버튼");
		if(flag1 == false){
			
			$("#inputPhone").attr('disabled',false);
			$(this).html("확인");
			flag1 = true;
		}else{
			
			$("#inputPhone").attr('disabled',true);
			$(this).html("수정");
			flag1 = false;
		}
		
		
	});
	var flag2 = false;
	
	$("#userNameBtn").click(function(){
		
		if(flag2 == false){
			
			$("#inputName").attr('disabled',false);
			$(this).html("확인");
			flag2 = true;
		}else{
			
			$("#inputName").attr('disabled',true);
			$(this).html("수정");
			flag2 = false;
		}
		
		
	});
	
</script>
</html>