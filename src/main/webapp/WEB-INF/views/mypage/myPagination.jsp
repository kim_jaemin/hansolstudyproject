<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>


  

	<div class="container" style="display: block; text-align: center">
		<div class="row">
			<div class="col">
				<ul class="pagination justify-content-center">
					<c:choose>
						<c:when test="${categoryInfo == null}">
							<c:if test="${paging.startPage != 1 }">
								<li class="page-item"><a class="page-link"
									href="/myPage?nowPage=${paging.startPage - 1}&cntPerPage=${paging.cntPerPage}">Previous</a>
								</li>
							</c:if>
							<c:forEach begin="${paging.startPage}" end="${paging.endPage}"
								var="p">
								<c:choose>
									<c:when test="${p == paging.nowPage}">
										<li class="page-item"><b class="page-link">${p}</b></li>
									</c:when>
									<c:when test="${p != paging.nowPage}">
										<li class="page-item"><a
											href="/myPage?nowPage=${p}&cntPerPage=${paging.cntPerPage}"
											class="page-link">${p}</a></li>
									</c:when>
								</c:choose>
							</c:forEach>
							<c:if test="${paging.endPage != paging.lastPage}">
								<li class="page-item"><a
									href="/myPage?nowPage=${paging.endPage + 1}&cntPerPage=${paging.cntPerPage}"
									class="page-link">Next</a></li>
							</c:if>
						</c:when>
						<c:otherwise>
							<c:if test="${paging.startPage != 1 }">
								<li class="page-item disabled"><a class="page-link"
									href="/myPageCategoryResult?categoryNo=${categoryInfo.categoryNo}&nowPage=${paging.startPage - 1}&cntPerPage=${paging.cntPerPage}">Previous</a>
								</li>
							</c:if>
							<c:forEach begin="${paging.startPage}" end="${paging.endPage}"
								var="p">
								<c:choose>
									<c:when test="${p == paging.nowPage}">
										<li class="page-item"><b class="page-link">${p}</b></li>
									</c:when>
									<c:when test="${p != paging.nowPage}">
										<li class="page-item"><a
											href="/myPageCategoryResult?categoryNo=${categoryInfo.categoryNo}&nowPage=${p}&cntPerPage=${paging.cntPerPage}"
											class="page-link">${p}</a></li>
									</c:when>
								</c:choose>
							</c:forEach>
							<c:if test="${paging.endPage != paging.lastPage}">
								<li class="page-item"><a
									href="/myPageCategoryResult?categoryNo=${categoryInfo.categoryNo}&nowPage=${paging.endPage + 1}&cntPerPage=${paging.cntPerPage}"
									class="page-link">Next</a></li>
							</c:if>
						</c:otherwise>
					</c:choose>
				</ul>
			</div>
		</div>
	</div>
