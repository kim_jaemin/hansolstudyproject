package com.example.StudyMoa.websocket.service.impl;

import java.util.HashMap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.StudyMoa.websocket.dao.WebSocketMapper;
import com.example.StudyMoa.websocket.dto.ChatRoom;
import com.example.StudyMoa.websocket.service.WebSocketService;

import lombok.extern.slf4j.Slf4j;


@Slf4j
@Service
public class WebSocketServiceImpl implements WebSocketService{
	
	@Autowired
	WebSocketMapper webSocketMapper;
	
	@Override
	public boolean insertChatRoom(ChatRoom room) {
	
		return webSocketMapper.insertChatRoom(room);
	}


	@Override
	public ChatRoom selectChatRoom(HashMap<Object, Object> params) {
		
		return webSocketMapper.selectChatRoom(params);
	}

}
