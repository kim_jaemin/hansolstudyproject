package com.example.StudyMoa.websocket.dao;

import java.util.HashMap;

import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface AlarmMapper {

	boolean insertAlarm(HashMap<String, Object> paramData);

}
