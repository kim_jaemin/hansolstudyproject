package com.example.StudyMoa.websocket.web;

import java.util.HashMap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.example.StudyMoa.websocket.service.AlarmService;

@Controller
public class AlarmController {

	@Autowired
	AlarmService alarmService;
	
	
	@RequestMapping(value="/insertAlarm",  method = {RequestMethod.POST}, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public boolean insertAlarm(@RequestBody HashMap<String,Object> paramData){
		boolean result = true;
		
		System.out.println("paramData : "+paramData);
		
		result = alarmService.insertAlarm(paramData);
		
		return result;
	}
	
}
