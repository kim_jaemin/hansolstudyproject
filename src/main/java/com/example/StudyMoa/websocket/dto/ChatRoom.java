package com.example.StudyMoa.websocket.dto;

import com.example.StudyMoa.common.dto.Category;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class ChatRoom {
	
	private int crNo;
	private int roomNumber;
	private String roomName;
	private int userNo;
	private int smNo;
	private int sNo;
	
	@Override
	public String toString() {
		return "ChatRoom [crNo=" + crNo + ", roomNumber=" + roomNumber + ", roomName=" + roomName + ", userNo=" + userNo
				+ ", smNo=" + smNo + ", sNo=" + sNo + "]";
	}
	
	
	
	
	
}
