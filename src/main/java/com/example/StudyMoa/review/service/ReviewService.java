package com.example.StudyMoa.review.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.example.StudyMoa.review.dto.Review;

public interface ReviewService {

	boolean insertStudyReview(Map review);

	List<Map> selectStudyReview(HashMap<String, Object> dataParam);

	double selectReviewGrade(Map<String, Object> map);

	HashMap<String, Object> selectStudyReviewOne(HashMap<String, Object> dataParam);

}
