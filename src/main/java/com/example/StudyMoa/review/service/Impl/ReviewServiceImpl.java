package com.example.StudyMoa.review.service.Impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.StudyMoa.review.dao.ReviewMapper;
import com.example.StudyMoa.review.dto.Review;
import com.example.StudyMoa.review.service.ReviewService;

@Service
public class ReviewServiceImpl implements ReviewService{

	@Autowired
	ReviewMapper reviewMapper;
	
	@Override
	public boolean insertStudyReview(Map review) {
	
		return reviewMapper.insertStudyReview(review);
	}

	@Override
	public List<Map> selectStudyReview(HashMap<String, Object> dataParam) {

		return reviewMapper.selectStudyReview(dataParam);
	}

	@Override
	public double selectReviewGrade(Map<String, Object> map) {
		
		return reviewMapper.selectReviewGrade(map);
	}

	@Override
	public HashMap<String, Object> selectStudyReviewOne(HashMap<String, Object> dataParam) {
		
		return reviewMapper.selectStudyReviewOne(dataParam);
	}

	
}
