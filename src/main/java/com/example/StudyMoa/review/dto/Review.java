package com.example.StudyMoa.review.dto;

import com.example.StudyMoa.common.dto.Category;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Review {

	private int srNo;
	private int sNo;
	private int srWriteDate;
	private int srUpdtDate;
	private String srContent;
	private double srGrade;
}
