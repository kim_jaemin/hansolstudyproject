package com.example.StudyMoa.comment.web;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.example.StudyMoa.comment.service.CommentService;
import com.example.StudyMoa.login.dto.User;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Controller
public class CommentController {
		
	@Autowired
	CommentService commentService;
	
	
	@RequestMapping(value="/insertStudyComment", method = {RequestMethod.POST}, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public boolean insertStudyComment (@RequestBody HashMap<String,Object> dataParam, @AuthenticationPrincipal User loginUser){
		boolean result = true;
			
		dataParam.put("userNo", loginUser.getUserNo());
		
		boolean insertCommentResult = commentService.insertStudyComment(dataParam);					//댓글 추가 서비스 메소드 동작
		
		
		return insertCommentResult;
	}
		
	@RequestMapping(value="/updateStudyComment")
	@ResponseBody
	public Map updateStudyComment(@RequestBody HashMap<String,Object> dataParam){
		boolean result = true;
		
		System.out.println("수정 : "+dataParam);
		
		
		boolean updateCommentResult = commentService.updateStudyComment(dataParam);					//댓글 수정 서비스 메소드 동작
		
		return dataParam;
	}
	
	@RequestMapping(value="/deleteStudyComment")
	@ResponseBody
	public boolean deleteStudyComment(@RequestBody HashMap<String,Object> dataParam){
		boolean result = true;
	
		boolean deleteCommentResult = commentService.deleteStudyComment(dataParam);					//댓글 삭제 서비스 메소드 동작
		System.out.println("deleteCommentResult : "+deleteCommentResult);
		
		return result;
	}
	
	
}
