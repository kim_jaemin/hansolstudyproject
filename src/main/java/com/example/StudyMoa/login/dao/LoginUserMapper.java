package com.example.StudyMoa.login.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.apache.ibatis.annotations.Mapper;
import org.springframework.data.repository.query.Param;

import com.example.StudyMoa.login.dto.User;

@Mapper
public interface LoginUserMapper {

	boolean insertUser(User user);
	
	Map findUserId(HashMap<String, Object> paramData);
	
	Map findUserPwd(HashMap<String, Object> paramData);

	int userIdCheck(HashMap<String, Object> paramData);
	
	User LoginIdCheck(String userId, String userPwd);		//아이디 조회

	boolean useredit(HashMap<String, Object> paramData);

	User checkeditPwd(HashMap<String, Object> paramData);

	boolean updateNewPwd(HashMap<String, Object> paramData);

	boolean deleteuser(HashMap<String, Object> paramData);

	Map selectNewPwd(HashMap<String, Object> paramData);

}
