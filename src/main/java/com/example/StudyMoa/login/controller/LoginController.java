package com.example.StudyMoa.login.controller;

import java.io.PrintWriter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.http.MediaType;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.example.StudyMoa.common.dto.Category;
import com.example.StudyMoa.common.service.CategoryService;
import com.example.StudyMoa.login.dto.User;
import com.example.StudyMoa.login.service.LoginUserService;
//import com.example.StudyMoa.login.service.impl.SecurityServiceImpl;



//LoginController : 로그인하여 이동하는 페이지 매핑(2021.10.27 김민정)

@Controller
@RequestMapping("/")
public class LoginController {




	@Autowired //LoginService대신 insertUserSerivce정의
	LoginUserService loginUserService;

	
	
	@Autowired
	CategoryService categoryService;

	
	//private final Logger logger = LoggerFactory.getLogger(LoginController.class.getName());			//다음 클래스로 넘어가는 클래스 이름 받아오기


	@GetMapping(value="/login")
	public String login(){
		return "login";


	}
	
	@RequestMapping(value="/mainPagesuccess")
	public String mainPagesuccess(){
		return "redirect:/mainPage";


	}

	@GetMapping(value="/join")
	//	@RequestMapping(value="/join", method = {RequestMethod.POST})				
	public String JoinPage(Model model, User user) throws Exception{

		System.out.println("join페이지로 들어옴");
		System.out.println("join페이지의 user정보 :"+user);
		List<Category> categoryList = categoryService.selectCategoryList();			//카테고리 리스트 조회
		model.addAttribute("categoryList", categoryList);							//카테고리 리스트 모델 객체 add

		return "joinPage";
	}

	@RequestMapping(value="/joinForm", method = {RequestMethod.GET, RequestMethod.POST})
	public String JoinPage(User user,Errors errors,Model model,HttpServletRequest request, HttpServletResponse response) throws Exception{ //joinPage() 파라미터 안의 정의된 객체정보가 담겨져있는 User클래스 객체 user에 대해 입력할 수 있는 정보를 찾고


		System.out.println("joinForm페이지로 들어옴");
		System.out.println("joinForm페이지로 들어옴 user정보: "+user);


		List<Category> categoryList = categoryService.selectCategoryList();			//카테고리 리스트 조회
		model.addAttribute("categoryList", categoryList);							//카테고리 리스트 모델 객체 add

		System.out.println("joinForm의 model정보 :"+model);


		boolean insertResult = loginUserService.insertUser(user);			//문제발생
		System.out.println("회원가입 결과 :"+insertResult);

		model.addAttribute("insertResult",insertResult);							//model에 객체를 담아서 jsp단에 정보를 전송하기

		if(insertResult == true) {


			response.setContentType("text/html; charset=UTF-8");					//java단에서 alert출력하기 [2021.11.18] 
			PrintWriter out = response.getWriter();
			out.println("<script> alert('회원가입이 완료되었습니다');</script>");
			out.flush(); 

			return "/joinSuccess";


		}else {
			return "/insertUserFail";
		}


	}
	
	
	//회원정보수정
	@RequestMapping(value="/useredit")
	public String useredit(){
		System.out.println("useredit진입");

		return "useredit"; // 원래는 loginPage.jsp로 해야되는데 viewResolver를 통해서 생략가능하다. => 생산성 향상 [2021.10.27 김민정]


	}
	
	@RequestMapping(value="/usereditForm", method = {RequestMethod.POST,RequestMethod.GET})		//아이디 유효성 검사
	@ResponseBody
	public boolean usereditForm(@RequestBody HashMap<String,Object> paramData,@AuthenticationPrincipal User loginUser){		

		System.out.println("회원정보 수정으로 들어온 값 : "+paramData);
		
		paramData.put("userNo",loginUser.getUserNo());
		boolean result = loginUserService.useredit(paramData);
		System.out.println("usereditForm controller return: "+result);
		return result;
	}
	
	@RequestMapping(value="/deleteuser", method = {RequestMethod.POST,RequestMethod.GET})		//아이디 유효성 검사
	@ResponseBody
	public boolean deleteuser(@RequestBody HashMap<String,Object> paramData){		

		System.out.println("회원정보 수정으로 들어온 값 : "+paramData);
		
		
		boolean result = loginUserService.deleteuser(paramData);
		System.out.println("usereditForm controller return: "+result);
		return result;
	}
	
	
	/*
	 * @RequestMapping(value="/usereditForm",method = {RequestMethod.GET,
	 * RequestMethod.POST}) //아이디 유효성 검사 public String usereditForm(User user){
	 * 
	 * System.out.println("회원정보 수정으로 들어온 값 : "+user);
	 * 
	 * 
	 * boolean result = loginUserService.useredit(user);
	 * 
	 * if(result == true) { return ""; }
	 * 
	 * }
	 */
	
	
	@RequestMapping(value="/checkeditPwd", method = RequestMethod.POST)		//회원정보 수정시 비밀번호 확인
	@ResponseBody
	public boolean checkeditPwd(@RequestBody HashMap<String,Object> paramData){		

		System.out.println("회원정보 수정으로 들어온 값 : "+paramData);
		

		boolean result = loginUserService.checkeditPwd(paramData);

		return result;
	}
	
	@RequestMapping(value="/checkvalidid", method = RequestMethod.POST)		//아이디 유효성 검사
	@ResponseBody
	public boolean checkvalidid(@RequestBody HashMap<String,Object> paramData){		

		System.out.println("checkvalidid에 필요한 ID값 : "+paramData);
		

		boolean result = loginUserService.checkvalidid(paramData);

		return result;
	}
	
	@RequestMapping(value="/checkvalidpwd", method = RequestMethod.POST)		//비밀번호 유효성 검사
	@ResponseBody
	public boolean checkvalidpwd(@RequestBody HashMap<String,Object> paramData){		

		System.out.println("checkvalidid에 필요한 PWD값 : "+paramData);
		

		boolean result = loginUserService.checkvalidpwd(paramData);
		System.out.println("패스워드 유효성 결과 :"+result);
		
		return result;
	}

	@RequestMapping(value="/userIdCheck", method = RequestMethod.POST)		//method를 Post로 하게되면 Get관련 오류발생, ajax는 post로 보내줬는
	@ResponseBody
	public int userIdCheck(@RequestBody HashMap<String,Object> paramData){		//jsp단에서 전송한 데이터를 해쉬맵 형태로 받아온다.

		System.out.println("userIdCheck 실행 : "+paramData);
		

		int result = loginUserService.userIdCheck(paramData);


		return result;
	}



	@RequestMapping(value="/findid")
	public String findid(){
		System.out.println("findId진입");

		return "findId"; // 원래는 loginPage.jsp로 해야되는데 viewResolver를 통해서 생략가능하다. => 생산성 향상 [2021.10.27 김민정]


	}
	

	@RequestMapping(value="/findidForm", method = {RequestMethod.GET, RequestMethod.POST})
	@ResponseBody
	public Map<String,Object> findidForm(@RequestBody HashMap<String,Object> paramData){

		System.out.println("findidForm 진입 : "+paramData);		

		Map userSelectOne = loginUserService.findUserId(paramData);							//userId를 찾는다
		System.out.println("전화번호를 통해 찾은 사용자의 아이디: "+userSelectOne);
		
		if(userSelectOne == null){
			Map<String, Object> selectOne = new HashMap<String,Object>();
			selectOne.put("USER_ID", null);
			return selectOne; 
		}
		else{
			return userSelectOne;
		}
	}


	@RequestMapping(value="/findpw")
	public String findpw(){

		System.out.println("findpw진입");

		return "findPw"; // 원래는 loginPage.jsp로 해야되는데 viewResolver를 통해서 생략가능하다. => 생산성 향상 [2021.10.27 김민정]


	}
	
	
	
	@RequestMapping(value="/updateNewPwd", method =  {RequestMethod.GET, RequestMethod.POST})
	@ResponseBody
	public Map<String, Object> updateNewPwd(@RequestBody HashMap<String, Object> paramData){

		System.out.println("updateNewPwd 진입"+paramData);
		
		//셀렉트 구문
//		String userName = (String)paramData.get("userName");
//		String userId = (String)paramData.get("userId");
		
//		String selectResultString = loginUserService.selectNewPwd(userName,userId);
//		Map selectResult = loginUserService.selectNewPwd(paramData);
//		System.out.println(selectResult);
//		
//		//selectResult.get("updateNewPwd0");
//		
//		if(selectResult == null) {												//디비에서 유저정보가 없다면 비밀번호를 변경할 수 없다.
//			Map<String, Object> selectPwdResult = new HashMap<String , Object>();
//			selectPwdResult.put("updateNewPwd0", null);
//			return selectPwdResult;
//		}else {
			
			Map updateNewPwd0 = loginUserService.updateNewPwd(paramData);
		
			System.out.println(updateNewPwd0);
			
			String pwd = (String)updateNewPwd0.get("updateNewPwd0");
			pwd = pwd.replaceAll("java.util.stream.SliceOps", "");
			
			updateNewPwd0.put("updateNewPwd0", pwd);
			System.out.println("임시 비밀번호 수정 결과 :"+updateNewPwd0.get("updateNewPwd0"));
			
			return updateNewPwd0;
//		}
		
		
		
	}
	
	
	//암호화한 임시 비밀번호 가져오기
	@RequestMapping(value="/findpwForm", method =  {RequestMethod.GET, RequestMethod.POST})
	@ResponseBody
	public Map<String, Object> findpwForm(@RequestBody HashMap<String, Object> paramData){

		System.out.println("findpwForm 진입"+paramData);

		
		Map userPwd = loginUserService.findUserPwd(paramData);
		
		System.out.println("아이디로 찾은 비밀번호 :"+userPwd);
		if(userPwd == null) {									//사용자의 비밀번호를 찾지 못한경우
			Map<String, Object> userPwdNull = new HashMap<String , Object>();
			userPwdNull.put("USER_PWD", null);
			return userPwdNull;
		}else {													//사용자의 비밀번호를 찾은경우

			return userPwd;
		}

		
		

	}


	@RequestMapping(value="/loginErrorPage")
	public String loginerrorpage() {


		return "errorPage";
	}

	@RequestMapping("/accessDenied") 
	public String accessDenied() { 
		return "accessDenied"; 
	}

	@RequestMapping(value="/logOut")
	public String logOut(HttpServletRequest request, HttpServletResponse response){
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		if(auth != null){
			new SecurityContextLogoutHandler().logout(request, response, auth);
		}
		return "redirect:/login";
	}



}
