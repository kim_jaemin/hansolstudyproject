package com.example.StudyMoa.common.dto;


import java.sql.Date;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Study {
	
	private int sNo;
	private String sTitle;
	private String sContent;
	private Date  sWriteDate;
	private Double bGrade;
	private int sTotal;
	private int sNow;
	private Date sUpdtDate;
	private int categoryNo;
	private int userNo;
	private String smRole;
	private String userName;
	private int smNo;
	private Date sEndDate;
	private String categoryThumbImg;
	private String recruitSdate;
	private String recruitEdate;
	private String classSdate;
	private String classEdate;
	private String classLocation;
	private String classTime;
	
	
	//평점 변환
	public void calcGradePercentage(Double bGrade){
		setBGrade((bGrade/5)*100);
	}


	@Override
	public String toString() {
		return "Study [sNo=" + sNo + ", sTitle=" + sTitle + ", sContent=" + sContent + ", sWriteDate=" + sWriteDate
				+ ", bGrade=" + bGrade + ", sTotal=" + sTotal + ", sNow=" + sNow + ", sUpdtDate=" + sUpdtDate
				+ ", categoryNo=" + categoryNo + ", userNo=" + userNo + ", smRole=" + smRole + ", userName=" + userName
				+ ", smNo=" + smNo + ", sEndDate=" + sEndDate + ", categoryThumbImg=" + categoryThumbImg
				+ ", recruitSdate=" + recruitSdate + ", recruitEdate=" + recruitEdate + ", classSdate=" + classSdate
				+ ", classEdate=" + classEdate + ", classLocation=" + classLocation + ", classTime=" + classTime + "]";
	}


	

	
}
