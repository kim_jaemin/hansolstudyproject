package com.example.StudyMoa.common.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.example.StudyMoa.comment.service.CommentService;
import com.example.StudyMoa.common.dto.Category;
import com.example.StudyMoa.common.dto.Paging;
import com.example.StudyMoa.common.dto.Study;
import com.example.StudyMoa.common.service.CategoryService;
import com.example.StudyMoa.common.service.StudyMemberService;
import com.example.StudyMoa.common.service.StudyService;
import com.example.StudyMoa.login.dto.User;
import com.example.StudyMoa.review.service.ReviewService;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Controller
public class DetailController {
	
	
	@Autowired
	StudyService studyService;
	
	@Autowired
	StudyMemberService studyMemberService;
	
	@Autowired
	CategoryService categoryService;
	
	@Autowired
	CommentService commentService;
	
	@Autowired
	ReviewService reviewService;
	
	@RequestMapping(value="/detailPage")
	public String goMainPage(Model model, 
							@RequestParam(value="sNo")int sNo
						   ,@RequestParam(value="smNo")int smNo, @AuthenticationPrincipal User loginUser) throws Exception{
		
		Map<String, Object> map = new HashMap<String, Object>();					             //Hash맵 객체 생성
		                                                                                         
		map.put("sNo", sNo);														             //Map에 sNo 푸시
		map.put("smNo", smNo);                                                                   
		Study study = studyService.selectStudyOne(map);								             //study 한개 조회해서 저장
		
		
		//List<Map> review = reviewService.selectStudyReview((HashMap<String, Object>) map);									//study 평균평점 DB조회
		
		double reviewGrade = reviewService.selectReviewGrade(map);
		study.calcGradePercentage(reviewGrade);													//평점 퍼센테이지 변환
		System.out.println("스터디 평점 : "+study.getBGrade());
		Category category = categoryService.selectCategoryOne(study.getCategoryNo());           //category 정보 조회 
		
		map.put("userNo",  loginUser.getUserNo());										                            //(Todo)  차후에 세션에 대한 정보 나중에 session userno값으로 넣어줘야함
		Map<String, Object> studyMemberYn = studyMemberService.selectStudyMember(map);
		System.out.println("studyMemberYn : "+studyMemberYn);
		map.put("categoryNo", study.getCategoryNo());
		map.put("startPage", 0);
		map.put("endPage", 4);
		map.put("smRole", "스터디장");
		
		List<Study> studyListPaging = studyService.selectStudyList(map);			             //페이징 내 스터디 리스트 조회
		
				
		
		List<Map> studyCommentList = commentService.selectCommentList(map);
		
		model.addAttribute("study", study);
		model.addAttribute("category", category);
		model.addAttribute("studyMemberYn", studyMemberYn);
		model.addAttribute("studyListPaging", studyListPaging);
		model.addAttribute("studyCommentList", studyCommentList);
		
		
		return "detail/detailPage";
	}
	
	@RequestMapping(value="/insertStudyMember", method = RequestMethod.POST)
	@ResponseBody
	public Study insertStudyMember(@RequestBody HashMap<String,Object> paramData, @AuthenticationPrincipal User loginUser) throws Exception{
			
		System.out.println("study ajax 통신 : "+paramData);
		
		int sNo = Integer.parseInt((String) paramData.get("studySNo"));
		int sNow = Integer.parseInt((String) paramData.get("studySNow"));
		int sTotal = Integer.parseInt((String) paramData.get("studySTotal"));
		int smNo = Integer.parseInt((String) paramData.get("studySmNo"));
		
		Map<String, Object> map = new HashMap<String, Object>();
		
		map.put("sNo", sNo);
		map.put("userNo", loginUser.getUserNo());
		map.put("smRole", "스터디원");
		map.put("smNo", smNo);
		
		System.out.println("map : "+map);
		
		boolean insertStudyMember = studyMemberService.insertStudyMember(map);
		
		//System.out.println("insertStudyMember :"+insertStudyMember);
		
		boolean updateSNowMember = studyService.updateStudySNow(sNo);
		
		Study study = studyService.selectStudyOne(map);
		
		System.out.println("study : " +study);
		
		
		return study;
	} 
	
	
	@RequestMapping(value="/selectStudyMember", method = RequestMethod.POST)
	@ResponseBody
	public List<Map> selectStudyMember(@RequestBody HashMap<String,Object> paramData) throws Exception{
	
		
		List<Map> studyMemberList = studyMemberService.selectStudyMemberList(paramData);
		
		return studyMemberList; 
	}

	@RequestMapping(value="/deleteStudy", method = RequestMethod.POST)
	@ResponseBody
	public boolean deleteStudy(@RequestBody HashMap<String,Object> paramData){
		boolean result = true;
		
		boolean deleteStudyMember = studyMemberService.deleteStudyMember(paramData);
		boolean deleteStudy = studyService.deleteStudy(paramData);
		
		return result;
	}  
	
	@RequestMapping(value="/exitStudy", method = RequestMethod.POST)
	@ResponseBody
	public boolean exitStudy(@RequestBody HashMap<String,Object> paramData){
		boolean result = true;
		
		result = studyMemberService.updateExitStudyMember(paramData);
		
		
		return result;
		}
	
	
	
	
	
	}  
