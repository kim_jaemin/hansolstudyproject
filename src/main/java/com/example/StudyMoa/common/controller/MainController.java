package com.example.StudyMoa.common.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.example.StudyMoa.common.dto.Category;
import com.example.StudyMoa.common.dto.Paging;
import com.example.StudyMoa.common.dto.Study;
import com.example.StudyMoa.common.service.CategoryService;
import com.example.StudyMoa.common.service.StudyService;
import com.example.StudyMoa.login.dto.User;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Controller
public class MainController {

	
	@Autowired
	StudyService studyService;	
	
	@Autowired
	CategoryService categoryService;

	//8080 처음 시작 페이지 설정
	/*
	 * @RequestMapping("/") public String StartLogin()throws Exception{ return
	 * "loginPage"; }
	 */
	   
	//mainPage 총 스터디 조회
	@RequestMapping(value="/mainPage")
	public String goMainPage(Model model, Paging paging
			,@RequestParam(value="nowPage", required = false) String nowPage
			,@RequestParam(value="cntPerPage", required = false) String cntPerPage
			,@RequestParam(value="keyword", required = false) String keyword
			,Authentication authentication) throws Exception {
		
		Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		User userDetails = (User) principal;
		
		System.out.println(" userDetails : "+userDetails);
		List<Category> categoryList = categoryService.selectCategoryList(); 		//카테고리 리스트 조회

		Map<String, Object> map = new HashMap<String, Object>();					//조회 파라미터로 넣을 MAP 생성
			
		if(nowPage == null && cntPerPage == null){									//RequestParam값이 null일 경우 자동 현재페이지 설정
			nowPage = "1";
			cntPerPage = "5";
		}else if(nowPage == null){
			nowPage = "1";
		}else if(cntPerPage == null){
			cntPerPage = "5";
		}
		
		map.put("keyword", keyword);
		
		map.put("categoryNo", null);												//category필터링 안했기 때문에 null값 
		map.put("smRole", "스터디장");													//스터디장의 스터디만 조회
		int total = studyService.selectStudyCount(map);								//스터디 갯수 조회
		
		paging = new Paging(total, Integer.parseInt(nowPage), Integer.parseInt(cntPerPage));
		
		map.put("startPage", paging.getStart());									//시작 페이징 세팅
		map.put("endPage", paging.getCntPerPage());									//페이지 당 보여줄 스터디 갯수
		map.put("smRole","스터디장");													//스터디장인 것만 조회
		System.out.println("paging정보 : "+paging);
		List<Study> studyListPaging = studyService.selectStudyList(map);			//페이징 내 스터디 리스트 조회
		

		model.addAttribute("userDetails", userDetails);
		model.addAttribute("paging", paging);
		model.addAttribute("list", studyListPaging);
		model.addAttribute("studySize", total);
		model.addAttribute("categoryList", categoryList);
		model.addAttribute("categoryInfo",null);
		
		return "main/mainPage";
	}

	//mainPage 필터링 스터디 조회
	@RequestMapping(value="/mainPageCategoryResult")
	public String goMainPageCategoryResult(@RequestParam("categoryNo") int categoryNo
			,@RequestParam(value="nowPage", required = false) String nowPage
			,@RequestParam(value="cntPerPage", required = false) String cntPerPage
			,@RequestParam(value="keyword", required = false) String keyword, Model model,  Paging paging) throws Exception {

		List<Category> categoryList = categoryService.selectCategoryList(); 		//카테고리 리스트 조회
		
		Map<String, Object> map = new HashMap<String, Object>();
		
		Category categoryInfo = categoryService.selectCategoryOne(categoryNo);		//카테고리 정보조회
		
		if(nowPage == null && cntPerPage == null){
			nowPage = "1";
			cntPerPage = "5";
		}else if(nowPage == null){
			nowPage = "1";
		}else if(cntPerPage == null){
			cntPerPage = "5";
		}
		map.put("categoryNo", categoryNo);
		map.put("keyword", keyword);
		map.put("smRole", "스터디장");
		int total = studyService.selectStudyCount(map);								//해당 카테고리 스터디 갯수 
		System.out.println("카테고리 후 total 갯수 : "+total);
		paging = new Paging(total, Integer.parseInt(nowPage), Integer.parseInt(cntPerPage));
		
		map.put("startPage", paging.getStart());
		map.put("endPage", paging.getCntPerPage());
									
		System.out.println("map2 : "+map + " , startPaging : "+paging.getStartPage()+", "+paging.getStart());
		List<Study> studyList = studyService.selectStudyList(map);					//카테고리 조건 스터디리스트 조회
		System.out.println("studyList Category조회 : "+studyList);		
		
		
		model.addAttribute("paging", paging);
		model.addAttribute("categoryInfo",categoryInfo);
		model.addAttribute("list", studyList);
		model.addAttribute("studySize", total);
		model.addAttribute("categoryList", categoryList);
		
		return "main/mainPage";	

		}
	

}