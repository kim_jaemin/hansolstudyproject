package com.example.StudyMoa.common.controller;

import java.io.File;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import com.example.StudyMoa.common.dto.Category;
import com.example.StudyMoa.common.service.AdminService;
import com.example.StudyMoa.common.service.CategoryService;
import com.example.StudyMoa.common.utils.UploadFileUtils;

@Controller
public class AdminController {

	@Autowired
	AdminService adminService;
	
	@Autowired
	CategoryService categoryService;
	
	//private String uploadPath="C:\\UploadImg";
	
	@Resource(name="uploadPath")
	private String uploadPath;
	
	
	@RequestMapping(value = "/adminCategory")
	public String adminCategory(){
		
		return "category/categoryInsertImg";
	}
	
	@RequestMapping(value = "/insertCategory", headers = ("content-type=multipart/*"))
	public String insertCategory(Category category, MultipartFile file) throws Exception{
		
		//System.out.println("file 정보 : "+file);
		String imgUploadPath = uploadPath + File.separator + "img";			//프로젝트에 파일 생성 되는 부분 경로
		//System.out.println("imgUploadPath : "+imgUploadPath);
		String ymdPath = UploadFileUtils.calcPath(imgUploadPath);
		//System.out.println("ymdPath : "+ymdPath);
		String fileName = null;
		
		if(file != null){
			//System.out.println("if문 실행 != null file 정보 : "+fileName);
			fileName = UploadFileUtils.fileUpload(imgUploadPath, file.getOriginalFilename(), file.getBytes(), ymdPath);
			//System.out.println("if문 != null file 정보 : "+fileName);
		}else {
			//System.out.println("if문 실행 == null file 정보 : "+fileName);
			fileName = uploadPath + File.separator + "images" + File.separator + "none.png";
			//System.out.println("if문 == null file 정보 : "+fileName);
		}
		
		
		category.setCategoryThumbImg(File.separator + "img" + ymdPath + File.separator + "s" + File.separator + "s_" + fileName);		
		
		
		boolean result = categoryService.insertCategory(category);
		
		
		
		return "redirect:/adminCategory";
	}
	
	@RequestMapping(value="/selectCategory")
	public String selectCategory(Model model) throws Exception{		
		
		//Category categoryInfo = categoryService.selectCategoryOne(categoryNo);		//카테고리 정보조회
		List<Category> categoryList = categoryService.selectCategoryList();
		
		
		//model.addAttribute("category", categoryInfo);
		model.addAttribute("categoryList", categoryList);
		
		
		
		return "category/categorySelect";
	}
	
	@RequestMapping(value="/adminPage")
	public String adminPage(){
		
		return "admin/adminPage";
	}
	
	@RequestMapping(value="modifyCaegory")
	public ModelAndView modifyCaegory() throws Exception{
	
		List<Category> categoryList = categoryService.selectCategoryList();
		
		ModelAndView mav = new ModelAndView();
		mav.addObject("categoryList", categoryList);
		mav.setViewName("detail/modifyStudy");
		
		return mav;
	}
	
	@RequestMapping(value="/selectInsertUserChart")
	@ResponseBody
	public List<Map<String, Object>> selectInsertUserChart(){
				
		List<Map<String,Object>> map = adminService.selectInsertUserChart();			//일주일 내 가입한 사용자 분포 조회
		
		return map;
	}
	
	@RequestMapping(value="/selectCategoryDistribution")
	@ResponseBody
	public List<Map<String, Object>> selectCategoryDistribution(){
		
		List<Map<String, Object>> map = adminService.selectCategoryDistribution();		//카테고리별 분포 조회
		
		return map;
	}
	
	@RequestMapping(value="/selectUserInfo")
	@ResponseBody
	public List<Map<String, Object>> selectUserInfo(){
		
		List<Map<String, Object>> map = adminService.selectUserInfo();					//유저 정보 조회
		
		return map;
	}
	
	@RequestMapping(value="/deleteUser")
	@ResponseBody
	public boolean deleteUser(@RequestBody HashMap<String,Object> dataParam){
		
		boolean result = adminService.deleteUser(dataParam);							//유저 탈퇴
	
		return result;
	}
	
	@RequestMapping(value="/selectCategoryAdmin")										
	@ResponseBody
	public List<Map<String, Object>> selectCategoryAdmin(){
		
		List<Map<String, Object>> map = adminService.selectCategoryAdmin();				//카테고리 리스트 조회
		
		return map;
	}	
	
	@RequestMapping(value="/deleteCategory")
	@ResponseBody
	public boolean deleteCategory(@RequestBody HashMap<String,Object> dataParam){
		
		boolean result = adminService.deleteCategory(dataParam);
		
		return result;
	}
	
	
}
