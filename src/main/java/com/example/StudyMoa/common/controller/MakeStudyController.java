package com.example.StudyMoa.common.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.example.StudyMoa.common.dao.MakeStudyMapper;
import com.example.StudyMoa.common.dto.Category;
import com.example.StudyMoa.common.dto.Study;
import com.example.StudyMoa.common.service.CategoryService;
import com.example.StudyMoa.common.service.MakeStudyService;
import com.example.StudyMoa.common.service.StudyService;
import com.example.StudyMoa.login.dto.User;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Controller
public class MakeStudyController {

	private MakeStudyMapper mapper;

	@Autowired
	MakeStudyService makeStudyService;
	
	@Autowired
	StudyService studyService;
	
	@Autowired
	CategoryService categoryService;
	
	//스터디 생성
	@RequestMapping(value="/makeStudy", method=RequestMethod.GET)
	public String insertStudy(Model model) throws Exception{
		
		List<Category> categoryList = categoryService.selectCategoryList(); 		//카테고리 리스트 조회
		model.addAttribute("categoryList", categoryList);
		
		
		//Category category = categoryService.selectCategoryOne(0);
		
		
		
		return "makeStudy";
	}
	
	// 로그인 정보 - 미완
//	@RequestMapping(value="/makestudyaction.do")
//	public String insertstudyaction(Model model) throws Exception{
//	model.addAttribute("userid");
//	return "redirect:/makeStudy";
//	
//		
//	}
	
	
	
	// 스터디 생성이 완료되면 뜨는 창
	@GetMapping("/createStudy")
	public String createStudy(Study study, User user,
			@AuthenticationPrincipal User loginUser){ // study, user 는 dto에서 가져옴
		
		Study studyInfo = new Study();
				
		//study.setCategoryNo(3);
		boolean studyResult = makeStudyService.insertMakeStudy(study);
		
		System.out.println("study 정보 : "+study);
		Map<String, Object> map = new HashMap<String, Object>();
		
		map.put("sNo", study.getSNo());
		map.put("userNo",loginUser.getUserNo());
		boolean resultStudyMember = makeStudyService.insertStudyLeader(map);
		
		return "redirect:/myPage";
	}
	

	// 스터디를 생성하지 않았을때 닫기 버튼을 누르면 메인페이지로 이동
	
	 @RequestMapping(value="/closeStudy") 
	 public String closeStudy(Model model){
	 return "redirect:/mainPage";			 
	 } 		 
			 
	 
	 	
	
}

