package com.example.StudyMoa.common.dao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface StudyMemberMapper {

	boolean insertStudyMember(Map<String, Object> map);

	Map<String, Object> selectStudyMember(Map<String, Object> map);

	List<Map> selectStudyMemberList(HashMap<String, Object> paramData);

	boolean deleteStudyMember(HashMap<String, Object> paramData);

	boolean updateExitStudyMember(HashMap<String, Object> paramData);

}
