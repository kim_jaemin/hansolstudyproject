package com.example.StudyMoa.common.dao;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface AbstractMapper {

	void printQueryId(String queryId);

	
	Object insert(String queryId, Object params);

	Object update(String queryId, Object params);
	
	Object delete(String queryId, Object params);
	
	Object selectOne(String queryId);
	
	Object selectOne(String queryId, Object params);
	
	@SuppressWarnings("rawstypes")
	List selectList(String queryId);
	
	@SuppressWarnings("rawstypes")
	List selectList(String queryId, Object params);
	
}
