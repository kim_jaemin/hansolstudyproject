package com.example.StudyMoa.common.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.StudyMoa.common.dao.CategoryMapper;
import com.example.StudyMoa.common.dto.Category;
import com.example.StudyMoa.common.service.CategoryService;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class CategoryServiceImpl implements CategoryService{
	
	
	@Autowired
	CategoryMapper categoryMapper;
	
	@Override
	public List<Category> selectCategoryList() throws Exception{
		
		return categoryMapper.selectCategoryList();
	}

	@Override
	public Category selectCategoryOne(int categoryNo) throws Exception {
		
		return categoryMapper.selectCategoryOne(categoryNo);
	}

	@Override
	public boolean insertCategory(Category category) {
		// TODO Auto-generated method stub
		return categoryMapper.insertCategory(category);
	}


}
