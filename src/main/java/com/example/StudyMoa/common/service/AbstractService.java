package com.example.StudyMoa.common.service;

import java.util.List;

public interface AbstractService {
	
	public void printQueryId(String queryId);
	
	public Object insert(String queryId, Object params);
	
	public Object update(String queryId, Object params);

	public Object delete(String queryId, Object params);
	
	public Object selectOne(String queryId);
	
	public Object selectOne(String queryId, Object params);
	
	@SuppressWarnings("rawtypes")
	public List selectList(String queryId);
	
	@SuppressWarnings("rawtypes")
	public List selectList(String queryId, Object params);
	
}
