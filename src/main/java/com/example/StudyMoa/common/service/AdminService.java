package com.example.StudyMoa.common.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.example.StudyMoa.common.dto.Category;

public interface AdminService {

	boolean insertCategory(Category category);					//카테고리 추가

	List<Map<String, Object>> selectInsertUserChart();			//최근 일주일 내 가입유저 조회

	List<Map<String, Object>> selectCategoryDistribution();		//카테고리별 분포 조회

	List<Map<String, Object>> selectUserInfo();					//유저정보 조회

	boolean deleteUser(HashMap<String, Object> dataParam);		//유저 탈퇴

	List<Map<String, Object>> selectCategoryAdmin();			//카테고리 리스트 조회

	boolean deleteCategory(HashMap<String, Object> dataParam);	//카테고리 삭제

}
