package com.example.StudyMoa.common.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.StudyMoa.common.dao.StudyMapper;
import com.example.StudyMoa.common.dto.Paging;
import com.example.StudyMoa.common.dto.Study;
import com.example.StudyMoa.common.service.StudyService;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class StudyServiceImpl implements StudyService{

	@Autowired
	StudyMapper studyMapper;
	
	@Override
	public List<Study> selectStudyList(Map<String, Object> map) throws Exception {
		
		return studyMapper.selectStudyList(map);
	}

	@Override
	public List<Study> selectStudyList(int categoryNo) throws Exception {
	
		return studyMapper.selectStudyList(categoryNo);
	}
	
	@Override
	public int selectStudyCount(Map<String, Object> map) throws Exception {

		return studyMapper.selectStudyCount(map);
	}

	@Override
	public Study selectStudyOne(Map<String, Object> map) throws Exception{
		
		return studyMapper.selectStudyOne(map);
	}

	@Override
	public boolean updateStudySNow(int sNow) {
	
		return studyMapper.updateStudySNow(sNow);
	}

	@Override
	public boolean updateStudySEndDate(HashMap<String, Object> reviewParam) {
		
		return studyMapper.updateStudyEndDate(reviewParam);
	}

	@Override
	public Double selectStudyOneGrade(Map<String, Object> map) {
	
		return studyMapper.selectStudyOneGrade(map);
	}

	@Override
	public boolean deleteStudy(HashMap<String, Object> paramData) {
		
		return studyMapper.deleteStudy(paramData);
	}




	

	
}
