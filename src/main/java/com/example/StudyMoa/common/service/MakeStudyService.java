package com.example.StudyMoa.common.service;

import java.util.List;
import java.util.Map;

import com.example.StudyMoa.common.dto.Study;
import com.example.StudyMoa.login.dto.User;

public interface MakeStudyService {

	//insert 했을때 성공했는지 안했는지 : boolean
	boolean insertMakeStudy(Study study);

	boolean insertStudyLeader(Map<String, Object> map);
	
	
	
}
