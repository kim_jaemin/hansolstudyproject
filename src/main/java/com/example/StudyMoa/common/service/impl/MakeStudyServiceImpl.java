package com.example.StudyMoa.common.service.impl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.StudyMoa.common.dao.MakeStudyMapper;
import com.example.StudyMoa.common.dto.Study;
import com.example.StudyMoa.common.service.MakeStudyService;
import com.example.StudyMoa.login.dto.User;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service

public class MakeStudyServiceImpl implements MakeStudyService{

	@Autowired 
	MakeStudyMapper makeStudyMapper;

	@Override
	public boolean insertMakeStudy(Study study) {
		
		
		return makeStudyMapper.insertMakeStudy(study);
	}


	@Override
	public boolean insertStudyLeader(Map<String, Object> map) {
		// TODO Auto-generated method stub
		return makeStudyMapper.insertStudyLeader(map);
	}
	
	
}
