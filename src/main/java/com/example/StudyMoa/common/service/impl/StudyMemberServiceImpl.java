package com.example.StudyMoa.common.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.StudyMoa.common.dao.StudyMemberMapper;
import com.example.StudyMoa.common.service.StudyMemberService;

import lombok.extern.slf4j.Slf4j;


@Slf4j
@Service
public class StudyMemberServiceImpl implements StudyMemberService{

	@Autowired
	StudyMemberMapper studyMemberMapper;
	
	
	@Override
	public boolean insertStudyMember(Map<String, Object> map) {
		
		return studyMemberMapper.insertStudyMember(map);
	}


	@Override
	public Map<String, Object> selectStudyMember(Map<String, Object> map) {
		
		return studyMemberMapper.selectStudyMember(map);
	}


	@Override
	public List<Map> selectStudyMemberList(HashMap<String, Object> paramData) {
		
		return studyMemberMapper.selectStudyMemberList(paramData);
	}


	@Override
	public boolean deleteStudyMember(HashMap<String, Object> paramData) {
		
		return studyMemberMapper.deleteStudyMember(paramData);
	}


	@Override
	public boolean updateExitStudyMember(HashMap<String, Object> paramData) {
		// TODO Auto-generated method stub
		return studyMemberMapper.updateExitStudyMember(paramData);
	}

}
