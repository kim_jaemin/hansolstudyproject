package com.example.StudyMoa.calendar.web;

import java.sql.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.example.StudyMoa.calendar.service.CalendarService;
import com.example.StudyMoa.login.dto.User;

import lombok.extern.slf4j.Slf4j;


@Slf4j
@Controller
public class CalendarController {

	@Autowired
	CalendarService calendarService;

	@RequestMapping("/showCalendar")
	public String goCalendar() {

		return "calendarPage";
	}

// 일정 조회 - 미완

	@RequestMapping("/selectFullCalendar")
	@ResponseBody
	public List<Map<String, Object>> goMyCalendar(Model model, @AuthenticationPrincipal User loginUser) throws Exception {

		Map<String, Object> myschedule = new HashMap<String, Object>();
		// 조회 파라미터로 넣을 MAP(schedule) 생성

		// 내 일정 조회하기 
		//myschedule.put("userNo", 3); //[session구현되면 수정할 부분]
		// System.out.println("map : "+myschedule);

		// List<Map<String, Object>> selectScheduleList =
		// calendarService.selectCalendarList(myschedule); //켈린더 정보 조회
		
		myschedule.put("userNo", loginUser.getUserNo());
		
		List<Map<String, Object>> selectScheduleListTest = calendarService.selectCalendarListTest(myschedule); // 켈린더 정보 조회 //사용자의 스터디 개수 조회
																										

		// List<Calendar> CalendarListPaging =
		// calendarService.selectCalendarList(myschedule); //페이징 내 유저의 스터디 리스트 조회

		model.addAttribute("list",selectScheduleListTest);

		
		System.out.println("list : "+selectScheduleListTest);
		
		return selectScheduleListTest;
	}

// calendar 일정 생성 - 미완


 @RequestMapping(value="/insertCalendarAjax", method = {RequestMethod.POST})
 @ResponseBody 
 public boolean insertSchedule (@RequestBody HashMap<String,Object> schedule, CalendarService calendarService){
 
 System.out.println("calendar 입력값 : "+schedule);
 

 // schedule.put("userNo", "3"); // sNO를 put해야함 -> sm
 
 boolean insertScheduleResult = calendarService.insertCalendar(schedule);
 //추가서비스 메소드 동작
 return insertScheduleResult;
 }


	// 캘린더 일정 생성 2 - makeStudy 참고
	
	
	
	
	
	
	
	
	// 일정 생성이 완료되면 뜨는 창

	
	  @GetMapping("/insertCalendar") 
	  public String createCalendar(@RequestParam ("title") String title, @RequestParam ("start") String start, @RequestParam ("end") String end
			  ,@AuthenticationPrincipal User loginUser){
		   HashMap<String,Object> schedule = new HashMap<String, Object>();
		   schedule.put("title", title);
		   schedule.put("start", start);
		   schedule.put("end", end);
		   schedule.put("userNo",loginUser.getUserNo());
		   //System.out.println("title :"+title+", start : "+start+", end : "+end);
		   boolean insertScheduleResult = calendarService.insertCalendar(schedule);
			

		   
	  return "redirect:/showCalendar"; 
	  }



// 일정 삭제


@GetMapping(value="/deleteCalendar")
public String removeCalendar(@RequestParam ("calNo") int calNo) {
	   Map<String,Object> schedule = new HashMap<String, Object>();
	   schedule.put("calNo", calNo);
	   boolean deleteScheduleResult = calendarService.deleteCalendar(schedule);
	
	   
	return "redirect:/showCalendar";
}


// 일정 수정


@GetMapping(value="/updateCalendar")
public String updateCalendar() {
	   Map<String,Object> schedule = new HashMap<String, Object>();
	   boolean updateScheduleResult = calendarService.updateCalendar(schedule);
		
	return "redirect:/showCalendar";
}
}

	 
