package com.example.StudyMoa.mypage.dao;

import org.apache.ibatis.annotations.Mapper;

import com.example.StudyMoa.login.dto.User;

@Mapper
public interface ProfileImgMapper {

	boolean insertProfileImg(User user);

	
}
