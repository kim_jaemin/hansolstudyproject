package com.example.StudyMoa.mypage.controller;

import java.io.File;
import java.io.IOException;

import javax.annotation.Resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.multipart.MultipartFile;

import com.example.StudyMoa.common.utils.UploadFileUtils;
import com.example.StudyMoa.login.dto.User;
import com.example.StudyMoa.mypage.service.ProfileImgService;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Controller
public class ProfileImgController {

	@Autowired
	ProfileImgService profileImgService;

	@Resource(name="uploadPath")
	private String uploadPath;
	

	@RequestMapping(value = "/insertUserProfileImgPage")
	public String adminCategory(){
		
		return "mypage/insertUserProfileImg";
	}
	
	@RequestMapping(value="/insertProfileImg")
	public String insertProfileImg ( User user, MultipartFile file, @AuthenticationPrincipal User loginUser, Model model) throws IOException, Exception{
		
		String imgUploadPath = uploadPath + File.separator + "img";			//프로젝트에 파일 생성 되는 부분 경로
		
		String ymdPath = UploadFileUtils.calcPath(imgUploadPath);
		
		String fileName = null;
		
		if(file != null){
			//System.out.println("if문 실행 != null file 정보 : "+fileName);
			fileName = UploadFileUtils.fileUpload(imgUploadPath, file.getOriginalFilename(), file.getBytes(), ymdPath);
			//System.out.println("if문 != null file 정보 : "+fileName);
		}else {
			//System.out.println("if문 실행 == null file 정보 : "+fileName);
			fileName = uploadPath + File.separator + "images" + File.separator + "none.png";
			//System.out.println("if문 == null file 정보 : "+fileName);
		}
		
		user.setUserNo(loginUser.getUserNo()); //차후에 Session으로 변경
		
		System.out.println("프로필 바꾸고 user 정보 : "+user+", 로그인정보 : "+loginUser);
		
		user.setUserProfileImg(File.separator + "img" + ymdPath + File.separator + "s" + File.separator + "s_" + fileName);
		
		boolean result = profileImgService.insertProfileImg(user);
		
		model.addAttribute("login", loginUser);
		
		return "redirect:/mainPage";
	}

	
	@RequestMapping("sidebar")
	public String sidebar (){
		
		return "mypage/sidebar";
	}

}
