package com.example.StudyMoa.mypage.service;

import java.util.List;
import java.util.Map;

import com.example.StudyMoa.common.dto.Study;
import com.example.StudyMoa.mypage.dto.MyPage;

// 1 class 를 interface 형태로 변환 1108 16:57
// 2 impl로 보내야함. 1108 16:57
public interface MyPageService {

	// mystudy 추가
	
	// 내 스터디 갯수 조회
	
	int selectMyStudyCount(Map<String, Object> map) throws Exception;

	// 내 스터디 목록 조회하기
	
	List<Map<String, Object>> selectMyStudyList(Map<String, Object> map) throws Exception;

	boolean updateStudySNow(int sNow);

	boolean updateStudyOne(Study study);

	// 스터디 생성시 스터디 인원 + 1
//	boolean updateStudySNow(int sNow);

	


}
