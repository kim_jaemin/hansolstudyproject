package com.example.StudyMoa.mypage.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.StudyMoa.login.dto.User;
import com.example.StudyMoa.mypage.dao.ProfileImgMapper;
import com.example.StudyMoa.mypage.service.ProfileImgService;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class ProfileImgServiceImpl implements ProfileImgService{
		
	@Autowired
	ProfileImgMapper profileImgMapper;
	
	@Override
	public boolean insertProfileImg(User user) {
	
		return profileImgMapper.insertProfileImg(user);
	}

}
