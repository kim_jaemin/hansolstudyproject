package com.example.StudyMoa;

import java.io.IOException;
import java.util.Objects;

import javax.sql.DataSource;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.session.SqlSessionFactory;
import org.mybatis.spring.SqlSessionFactoryBean;
import org.mybatis.spring.SqlSessionTemplate;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.objenesis.strategy.PlatformDescription;
import org.springframework.transaction.PlatformTransactionManager;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;

@Configuration
@MapperScan(basePackages = "com.example.StudyMoa.*.dao")
@PropertySource("classpath:/application.properties")
public class DBconfig {
	
	@Autowired
	ApplicationContext applicationContext;
	
	@Bean
	@ConfigurationProperties(prefix= "spring.datasource.hikari")
	public HikariConfig hikariConfig(){
		return new HikariConfig();
	}
	
	@Bean
	public DataSource dataSource(){
		return new HikariDataSource(hikariConfig());
	}
	
	@Bean
	public SqlSessionFactory sqlSessionFactory(DataSource dataSource) throws Exception{
		SqlSessionFactoryBean bean = new SqlSessionFactoryBean();
		
		bean.setDataSource(dataSource);
		bean.setMapperLocations(applicationContext.getResources("classpath:/mybatis-mapper/*.xml"));	//mapper path
		bean.setTypeAliasesPackage("com.example.StudyMoa.dto");											//alias
		Objects.requireNonNull(bean.getObject()).getConfiguration().setMapUnderscoreToCamelCase(true);	//camelCase
		
		return bean.getObject();		
	}
	/*
	 * sqlSession
	 * */
	public SqlSessionTemplate sqlSessionTemplate(SqlSessionFactory sqlSessionFactory){
		return new SqlSessionTemplate(sqlSessionFactory);
	}
	
	/*
	 * transaction manager
	 * */
	@Bean(name = "txManager")
	public PlatformTransactionManager txManager(@Qualifier("dataSource") DataSource dataSource){
		DataSourceTransactionManager dataSourceTransactionManager = new DataSourceTransactionManager(dataSource);
		dataSourceTransactionManager.setNestedTransactionAllowed(true);	//nested
		return dataSourceTransactionManager;
	}
	
}
